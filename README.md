# FreeRTOS学习

#### 介绍
FreeRTOS学习笔记，以**官方的25个例程**为主要内容，快速学习FreeRTOS的各种功能。

本笔记中使用到的单片机硬件，成本仅20元左右。

#### 硬件需求
1.一台Windows电脑。

2.STM32最小系统板(含C8T6大概10块钱)

![输入图片说明](https://foruda.gitee.com/images/1693969417143986758/31096016_1733933.png "屏幕截图")

3.DAP下载器

推荐创芯工坊的PowerWriter PWLINK2。

实际上他们亏本出售，只是为了推广，步进价格便宜，顺丰包邮，甚至发空运邮寄。

![输入图片说明](https://foruda.gitee.com/images/1693969587707870098/30a94d7e_1733933.png "屏幕截图")

#### 软件需求

1.Keil(V5)

#### 使用说明

本仓库包含30个分支，其中25个分支名为“Examplexxx”，基于FreeRTOS官方文档提供的例程修改。

![输入图片说明](https://foruda.gitee.com/images/1693971021993895220/3009f5d9_1733933.png "屏幕截图")

其中分支BaseVersion是构建完成的STM32F103裸机工程，包含了串口输入输出功能。

分支BaseVersion01完成FreeRTOS的移植，其他例程分支都是在此版本上添加例程代码。

另有2个分支Example_StreamBuffer、Example_MessageBuffer,是笔者基于FreeRTOS v10新增的功能特性添加。

每个分支，都配有MarkDown文件，详细分析该例程的程序代码，并总结例程的知识要点。

在master分支，可以查看所有29篇学习笔记。

![输入图片说明](https://foruda.gitee.com/images/1693971197744943979/57968096_1733933.png "屏幕截图")

学习时，可根据需要，切换到相应分支，也可以通过下面链接跳转。

[Example001-任务的创建](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example001/)

[Example002-使用任务参数](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example002/)

[Example003-优先级实验](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example003/)

[Example004-使用“阻塞”状态产生延时](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example004/)

[Example005-使用vTaskDelayUntil()产生延时](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example005/)

[Example006-阻塞任务和非阻塞任务的组合](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example006/)

[Example007-定义空闲任务钩子函数](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example007/)

[Example008-改变任务优先级](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example008/)

[Example009-删除任务](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example009/)

[Example010-从队列接收时阻塞](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example010/)

[Example011-发送到队列时阻塞，通过队列发送结构体](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example011/)

[Example012-使用队列集](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example012/)

[Example013-创建单次定时器和自动加载定时器](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example013/)

[Example014-使用回调函数参数，和软件定时器ID](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example014/)

[Example015-复位软件定时器](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example015/)

[Example016-使用二值信号量实现任务和中断的同步](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example016/)

[Example017-使用计数信号量实现任务和中断的同步](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example017/)

[Example018-集中延迟中断处理](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example018/)

[Example019-在中断中发送和接收队列](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example019/)

[Example020-用信号量重写vPrintString()函数](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example020/)

[Example021-用看守任务重写vPrintString函数](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example021/)

[Example022-事件组实验](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example022/)

[Example023-同步任务](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example023/)

[Example024-使用任务通知代替信号量-方法1](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example024/)

[Example025-使用任务通知代替信号量-方法2](http://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example025/)

[Example_StreamBuffer-使用StreamBuffer发送和接收数据](https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example_StreamBuffer/)

[Example_MessageBuffer-使用MessageBuffer发送和接收消息](https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example_MessageBuffer/)
