## FreeRTOS学习笔记[10]Example008-改变任务优先级(Lu)

本篇主要是学习动态改变任务的优先级。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example008

把其中的main.c文件改为Example008.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example008分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example008/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example008

主要修改：

1.Example008.c中的main()函数名称改为 Example008()，然后在工程main.c文件中声明，并在初始化完成后调用 Example008()；

2.任务栈大小从1000改为512，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example008.c添加到工程，然后编译。

下载运行，会持续输出字符串。

![1680877057349](image\1680877057349.png)

#### Example008代码分析

```
void vTask1( void *pvParameters )
{
UBaseType_t uxPriority;

	/* 此任务将始终在 Task2 之前运行，因为它具有更高的优先级。 
	Task1 和 Task2 都不会阻塞，因此两者都将始终处于 Running 或 Ready 状态。

	查询该任务运行的优先级——传入 NULL 表示“返回自己的优先级”。 */
	uxPriority = uxTaskPriorityGet( NULL );

	for( ;; )
	{
		/* 打印出此任务的名称。 */
		vPrintString( "Task1 is running\r\n" );

		/* 将 Task2 的优先级设置为高于 Task1 的优先级将导致 Task2 立即开始运行
		（因为这样 Task2 将具有两个已创建任务的更高优先级）。 */
		vPrintString( "About to raise the Task2 priority\r\n" );
		vTaskPrioritySet( xTask2Handle, ( uxPriority + 1 ) );

		/* Task1 只有在其优先级高于 Task2 时才会运行。 
		因此，要让此任务到达这一点，Task2 必须已经执行并将其优先级设置回 0。 */
	}
}
```

1.在该函数中，首先获取了任务1的优先级。

2.打印输出两个字符串。

3.设置任务2的优先级等于任务1的优先级+1，即提高任务2的优先级为3。



```
void vTask2( void *pvParameters )
{
UBaseType_t uxPriority;

	/* 任务 1 将始终在此任务之前运行，因为任务 1 具有更高的优先级。 
	Task1 和 Task2 都不会阻塞，因此将始终处于 Running 或 Ready 状态。

	查询该任务运行的优先级——传入 NULL 表示“返回自己的优先级”。 */
	uxPriority = uxTaskPriorityGet( NULL );

	for( ;; )
	{
		/* 要使此任务到达此点，Task1 必须已经运行并将此任务的优先级设置为高于它自己的优先级。

		打印出此任务的名称。 */
		vPrintString( "Task2 is running\r\n" );

		/* 将的优先级设置回其原始值。 传入 NULL 作为任务句柄意味着“改变自己的优先级”。 
		将优先级设置为低于 Task1 将导致 Task1 立即重新开始运行。 */
		vPrintString( "About to lower the Task2 priority\r\n" );
		vTaskPrioritySet( NULL, ( uxPriority - 2 ) );
	}
}
```

1.首先获取了任务2的优先级。

2.打印输出两个字符串。

3.设置当前任务的优先级-2，即降低任务2的优先级为1。



```
/* 用于持有任务 2 的句柄。 */
TaskHandle_t xTask2Handle;

/*-----------------------------------------------------------*/

int Example008( void )
{
	/* 创建优先级为 2 的第一个任务。这次任务参数不使用，设置为 NULL。 
	任务句柄也未使用，因此同样也设置为 NULL。 */
	xTaskCreate( vTask1, "Task 1", 512, NULL, 2, NULL );
          					/* 该任务以优先级 2 ^ 创建。 */

	/* 以优先级 1 创建第二个任务 - 低于赋予 Task1 的优先级。 
	再次没有使用任务参数，因此设置为 NULL - 
	但是这次想要获得任务的句柄，因此传入 xTask2Handle 变量的地址。 */
	xTaskCreate( vTask2, "Task 2", 512, NULL, 1, &xTask2Handle );
        					/* 任务句柄是最后一个参数 ^^^^^^^^^^ */

	/* 启动调度程序以开始执行任务。 */
	vTaskStartScheduler();	

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

例程入口函数，创建了2个任务后，启动调度器。此时Task 1比Task 2优先级高。

创建任务2的时候，通过xTaskCreate()函数的最后一个参数，返回了任务的句柄。

本例中，通过任务句柄修改任务2的优先级。



**知识点**：

1.获取任务优先级的函数为uxTaskPriorityGet()，传入NULL获取的是当前任务优先级。

2.设置任务优先级的函数为vTaskPrioritySet()，传入NULL设置的是当前任务优先级。

3.通过配置INCLUDE_vTaskPrioritySet为1，使能设置任务优先级的函数。

4.通过配置INCLUDE_uxTaskPriorityGet为1，使能获取任务优先级的函数。



The End.

2023-04-07.