## FreeRTOS学习笔记[11]Example009-删除任务(Lu)

本篇主要是学习如何删除一个任务。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example009

把其中的main.c文件改为Example009.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example009分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example009/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example009

主要修改：

1.Example009.c中的main()函数名称改为 Example009()，然后在工程main.c文件中声明，并在初始化完成后调用 Example009()；

2.任务栈大小从1000改为512，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example009.c添加到工程，然后编译。

下载运行，会持续输出字符串。

![1680877665826](image\1680877665826.png)

#### Example009代码分析

```
void vTask1( void *pvParameters )
{
const TickType_t xDelay100ms = pdMS_TO_TICKS( 100UL );

	for( ;; )
	{
		/* 打印出此任务的名称。 */
		vPrintString( "Task1 is running\r\n" );

		/* 以更高的优先级创建任务 2。 再次没有使用任务参数，因此设置为 NULL - 
        但是这次想要获得任务的句柄，因此传入 xTask2Handle 变量的地址。 */
		xTaskCreate( vTask2, "Task 2", 512, NULL, 2, &xTask2Handle );
        					    /* 任务句柄是最后一个参数 ^^^^^^^^^^ */

		/* 任务 2 具有更高的优先级，因此任务 1 要到达此处，任务 2 必须已经执行并自行删除。 
        延迟 100 毫秒。 */
		vTaskDelay( xDelay100ms );
	}
}
```

1.打印输出一个字符串。

2.在任务1中创建任务2，通过xTaskCreate()函数的最后一个参数，返回了任务的句柄。后面通过该任务句柄删除任务2。

3.延时100ms。



```
void vTask2( void *pvParameters )
{
	/* Task2 除了删除自身之外什么都不做。 
    为此，它可以使用 NULL 参数调用 vTaskDelete()，
    但纯粹出于演示目的，它使用自己的任务句柄调用 vTaskDelete()。 */
	vPrintString( "Task2 is running and about to delete itself\r\n" );
	vTaskDelete( xTask2Handle );
}
```

1.打印输出一个字符串。

2.在任务2函数中，把任务2删除。



```
/* 用于持有任务 2 的句柄。 */
TaskHandle_t xTask2Handle;

/*-----------------------------------------------------------*/

int Example009( void )
{
	/* 创建优先级为 1 的第一个任务。这次任务参数不使用，设置为 NULL。 
    任务句柄也未使用，因此同样也设置为 NULL。 */
	xTaskCreate( vTask1, "Task 1", 512, NULL, 1, NULL );
          					/* 该任务以优先级 1 ^ 创建。 */

	/* 启动调度程序以开始执行任务。 */
	vTaskStartScheduler();	

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

例程入口函数，创建了1个任务，优先级为1，然后启动调度器。



**知识点**：

1.删除任务的函数为vTaskDelete()，传入NULL删除的是当前任务优先级。

2.通过配置INCLUDE_vTaskDelete为1，使能任务删除函数。



The End.

2023-04-07.