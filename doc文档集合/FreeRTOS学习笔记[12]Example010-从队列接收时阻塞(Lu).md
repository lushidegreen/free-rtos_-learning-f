## FreeRTOS学习笔记[12]Example010-从队列接收时阻塞(Lu)

本篇主要是如何使用队列发送和接收数据。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example010

把其中的main.c文件改为Example010.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example010分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example010/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example010

主要修改：

1.Example010.c中的main()函数名称改为 Example010()，然后在工程main.c文件中声明，并在初始化完成后调用 Example010()；

2.任务栈大小从1000改为256，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example010.c添加到工程，然后编译。

下载运行，会持续输出字符串。

![1680957607528](image\1680957607528.png)

#### Example010代码分析

```
/* 声明一个 QueueHandle_t 类型的变量。 这用于存储所有三个任务都访问的队列。 */
QueueHandle_t xQueue;


int Example010( void )
{
    /* 创建队列以最多保存 5 个long值。 */
    xQueue = xQueueCreate( 5, sizeof( int32_t ) );

	if( xQueue != NULL )
	{
		/* 创建将写入队列的任务的两个实例。 该参数用于传递任务应写入队列的值，
		因此一个任务将连续向队列写入 100，而另一个任务将向队列连续写入 200。 
		这两个任务都以优先级 1 创建。 */
		xTaskCreate( vSenderTask, "Sender1", 256, ( void * ) 100, 1, NULL );
		xTaskCreate( vSenderTask, "Sender2", 256, ( void * ) 200, 1, NULL );

		/* 创建将从队列中读取的任务。 该任务以优先级 2 创建，因此高于发送者任务的优先级。 */
		xTaskCreate( vReceiverTask, "Receiver", 256, NULL, 2, NULL );

		/* 启动调度程序，以便开始执行创建的任务。 */
		vTaskStartScheduler();
	}
	else
	{
		/* 无法创建队列。 */
	}

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建了一个队列，可容纳5个int32数据。

创建2个非阻塞的发送任务，优先级都是1，并且使用同一个实现函数，通过任务参数传递要发送到队列的数据；

创建1个接收任务，优先级为2，比发送任务高。

然后启动OS执行任务。



```
static void vSenderTask( void *pvParameters )
{
int32_t lValueToSend;
BaseType_t xStatus;

	/* 该任务创建了两个实例，因此发送到队列的值通过任务参数传入，而不是硬编码。 
	这样每个实例都可以使用不同的值。 将参数转换为所需的类型。 */
	lValueToSend = ( int32_t ) pvParameters;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 第一个参数是数据发送到的队列。 队列是在调度程序启动之前创建的，所以在这个任务开始执行之前。

		第二个参数是要发送的数据的地址。

		第三个参数是阻塞时间——如果队列已经满了，任务应该保持在阻塞状态以等待队列中可用空间的时间。 
		在这种情况下，没有指定阻塞时间，因为队列中应该总是有空间。 */
		xStatus = xQueueSendToBack( xQueue, &lValueToSend, 0 );

		if( xStatus != pdPASS )
		{
			/* 无法写入队列，因为它已满——这一定是一个错误，因为队列不应该包含超过一个项目！ */
			vPrintString( "Could not send to the queue.\r\n" );
		}
	}
}
```

任务功能很简单，就是把发送消息到队列。

发送的数据，是创建任务时传入的参数。任务1发送的是100，任务2发送的是200。



```
static void vReceiverTask( void *pvParameters )
{
/* 声明将保存从队列接收到的值的变量。 */
int32_t lReceivedValue;
BaseType_t xStatus;
const TickType_t xTicksToWait = pdMS_TO_TICKS( 100UL );

	/* 此任务也在无限循环中定义。 */
	for( ;; )
	{
		/* 由于此任务会立即解除阻塞数据写入队列，因此此调用应始终发现队列为空。 */
		if( uxQueueMessagesWaiting( xQueue ) != 0 )
		{
			vPrintString( "Queue should have been empty!\r\n" );
		}

		/* 第一个参数是要从中接收数据的队列。队列是在调度程序启动之前创建的，因此在此任务第一次运行之前。

		第二个参数是将接收到的数据放入的缓冲区。此时，缓冲区只是具有保存接收数据所需大小的变量的地址。

		最后一个参数是阻塞时间——如果队列已经为空，任务应该保持在阻塞状态以等待数据可用的最长时间。 */
		xStatus = xQueueReceive( xQueue, &lReceivedValue, xTicksToWait );

		if( xStatus == pdPASS )
		{
			/* 从队列中成功接收到数据，打印出接收到的值。 */
			vPrintStringAndNumber( "Received = ", lReceivedValue );
		}
		else
		{
			/* 即使在等待 100 毫秒后，也没有从队列中收到任何内容。 
			这一定是一个错误，因为发送任务是自由运行的，并且会不断地写入队列。 */
			vPrintString( "Could not receive from the queue.\r\n" );
		}
	}
}
```

任务收到消息并打印输出，又从队列接收消息，进入阻塞状态。

在本例中，接收任务优先级最高，所以当队列中有消息后，会立刻被接收任务出来。

所以每次运行到if( uxQueueMessagesWaiting( xQueue ) != 0 )，都不成立。



**知识点**：

1.创建队列使用宏xQueueCreate()，实际调用的是xQueueGenericCreate()函数。

2.发送消息到队列尾部使用宏xQueueSendToBack()，实际调用的是xQueueGenericSend()函数。

3.相应的，发送消息到队列头部使用宏xQueueSendToFront()，实际调用函数也是xQueueGenericSend()。

4.从队列接收消息，使用函数xQueueReceive()。

5.通过uxQueueMessagesWaiting()函数，可以获取当前队列中的消息数量。



The End.

2023-04-08.