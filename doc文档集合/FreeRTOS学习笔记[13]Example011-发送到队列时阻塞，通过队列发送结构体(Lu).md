## FreeRTOS学习笔记[13]Example011-发送到队列时阻塞，通过队列发送结构体(Lu)

本篇主要是学习如何使用队列发送和接收结构体。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example011

把其中的main.c文件改为Example011.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example011分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example011/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example011

主要修改：

1.Example011.c中的main()函数名称改为 Example011()，然后在工程main.c文件中声明，并在初始化完成后调用 Example011()；

2.任务栈大小从1000改为256，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example011.c添加到工程，然后编译。

下载运行，会持续输出字符串。

![1680958735493](image\1680958735493.png)

#### Example011代码分析

```
/* 声明一个 QueueHandle_t 类型的变量。 这用于存储所有三个任务都访问的队列。 */
QueueHandle_t xQueue;

typedef enum
{
	eSender1,
	eSender2
} DataSource_t;

/* 定义将在队列中传递的结构类型。 */
typedef struct
{
	uint8_t ucValue;
	DataSource_t eDataSource;
} Data_t;

/* 声明两个将在队列中传递的 Data_t 类型的变量。 */
static const Data_t xStructsToSend[ 2 ] =
{
	{ 100, eSender1 }, /* Sender1使用。 */
	{ 200, eSender2 }  /* Sender2使用。 */
};

int Example011( void )
{
    /* 创建队列以容纳最多 3 个 Data_t 类型的结构。 */
    xQueue = xQueueCreate( 3, sizeof( Data_t ) );

	if( xQueue != NULL )
	{
		/* 创建将写入队列的任务的两个实例。 该参数用于传递任务应该写入队列的结构，
		因此一个任务会不断地向队列发送xStructsToSend[0]，另一个任务会不断地发送xStructsToSend[1]。 
		两个任务都以优先级 2 创建，高于接收者的优先级。 */
	xTaskCreate( vSenderTask, "Sender1", 256, (void *) &(xStructsToSend[0]), 2, NULL);
	xTaskCreate( vSenderTask, "Sender2", 256, (void *) &(xStructsToSend[1]), 2, NULL);

		/* 创建将从队列中读取的任务。 该任务以优先级 1 创建，因此低于发送者任务的优先级。 */
		xTaskCreate( vReceiverTask, "Receiver", 256, NULL, 1, NULL );

		/* 启动调度程序，以便开始执行创建的任务。 */
		vTaskStartScheduler();
	}
	else
	{
		/* 无法创建队列。 */
	}

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建了一个队列，可容纳3个Data_t型结构体数据。

创建2个非阻塞的发送任务，优先级都是2，并且使用同一个实现函数，通过任务参数传递要发送到队列的结构体数据；

创建1个接收任务，优先级为1。

然后启动OS执行任务。



```
static void vSenderTask( void *pvParameters )
{
BaseType_t xStatus;
const TickType_t xTicksToWait = pdMS_TO_TICKS( 100UL );

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 第一个参数是数据发送到的队列。 队列是在调度程序启动之前创建的，所以在这个任务开始执行之前。

		第二个参数是要发送的结构的地址。 地址作为任务参数传入。

		第三个参数是阻塞时间——如果队列已经满，任务应该保持在阻塞状态以等待队列上可用空间的时间。 
		指定阻塞时间，因为队列将变满。 只有当两个发送任务都处于阻塞状态时，项目才会从队列中删除。 */
		xStatus = xQueueSendToBack( xQueue, pvParameters, xTicksToWait );

		if( xStatus != pdPASS )
		{
			/* 无法写入队列，因为它已满 - 这一定是一个错误，
			因为一旦两个发送任务都处于阻塞状态，接收任务就应该在队列中腾出空间。 */
			vPrintString( "Could not send to the queue.\r\n" );
		}
	}
}
```

任务功能很简单，就是把发送消息到队列。

发送的数据，是创建任务时传入的参数。任务1发送的是xStructsToSend[0]，任务2发送的是xStructsToSend[1]。

注意，两个发送任务都是非阻塞的，不会主动放弃CPU。只有队列满后，发送数据到队列导致阻塞，才会让出CPU。



```
static void vReceiverTask( void *pvParameters )
{
/* 声明将保存从队列接收到的值的结构。 */
Data_t xReceivedStructure;
BaseType_t xStatus;

	/* 此任务也在无限循环中定义。 */
	for( ;; )
	{
		/* 由于此任务仅在发送任务处于阻塞状态时运行，并且发送任务仅在队列已满时才阻塞，
		因此此任务应始终查找队列已满。 3 是队列长度。 */
		if( uxQueueMessagesWaiting( xQueue ) != 3 )
		{
			vPrintString( "Queue should have been full!\r\n" );
		}

		/* 第一个参数是要从中接收数据的队列。队列是在调度程序启动之前创建的，因此在此任务第一次运行之前。

		第二个参数是将接收到的数据放入的缓冲区。此时，缓冲区只是具有保存接收结构所需大小的变量的地址。

		最后一个参数是阻塞时间——如果队列已经为空，任务应该保持在阻塞状态以等待数据可用的最长时间。 
		不需要阻塞时间，因为此任务仅在队列已满时运行，因此数据将始终可用。 */
		xStatus = xQueueReceive( xQueue, &xReceivedStructure, 0 );

		if( xStatus == pdPASS )
		{
			/* 从队列中成功接收到数据，打印出接收到的值和值的来源。 */
			if( xReceivedStructure.eDataSource == eSender1 )
			{
				vPrintStringAndNumber( "From Sender 1 = ", xReceivedStructure.ucValue );
			}
			else
			{
				vPrintStringAndNumber( "From Sender 2 = ", xReceivedStructure.ucValue );
			}
		}
		else
		{
			/* 没有从队列中收到任何东西。 这一定是一个错误，因为此任务仅应在队列已满时运行。 */
			vPrintString( "Could not receive from the queue.\r\n" );
		}
	}
}
```

任务收到消息，根据消息内容，打印输出发送消息的任务信息，以及消息内容。

 在本例中，两个发送任务优先级都比接收任务高，所以只有队列满时、发送任务都进入阻塞状态后，接收任务才得到运行。

而且，当接收任务读取1个消息后，队列就处于未满状态，发送任务立刻又发送消息到队列中。

所以每次运行到if( uxQueueMessagesWaiting( xQueue ) != 3 )，都不成立。



**知识点**：

1.发送数据到队列，其动作是copy数据到队列中。

2.从队列接收数据，其动作是从队列中copy数据出来。

3.发送或接收队列数据时，copy数据的长度，根据xQueueCreate()创建队列时的第二个参数确定。



The End.

2023-04-08.