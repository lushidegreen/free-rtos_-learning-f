## FreeRTOS学习笔记[14]Example012-使用队列集(Lu)

本篇主要是学习如何使用队列集管理接收多个队列/信号量。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example012

把其中的main.c文件改为Example012.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example012分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example012/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example012

主要修改：

1.Example012.c中的main()函数名称改为 Example012()，然后在工程main.c文件中声明，并在初始化完成后调用 Example012()；

2.任务栈大小从1000改为256，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example012.c添加到工程，然后编译。

下载运行，会持续输出字符串。

![1680962983295](image\1680962983295.png)

#### Example012代码分析

```
/* 声明 QueueHandle_t 类型的两个变量。 两个队列都添加到同一个队列集中。 */
static QueueHandle_t xQueue1 = NULL, xQueue2 = NULL;

/* 声明一个 QueueSetHandle_t 类型的变量。 这是添加两个队列的队列集。 */
static QueueSetHandle_t xQueueSet = NULL;

int Example012( void )
{
	/* 创建两个队列。 每个队列发送字符指针。 
	接收任务的优先级高于发送任务的优先级，因此队列在任何时候都不会超过一项。 */
    xQueue1 = xQueueCreate( 1, sizeof( char * ) );
	xQueue2 = xQueueCreate( 1, sizeof( char * ) );

	/* 创建队列集。 有两个队列都可以包含 1 个项目，
	因此队列集必须持有的最大队列句柄数为 2（1 个项目乘以 2 个集合）。 */
	xQueueSet = xQueueCreateSet( 1 * 2 );

	/* 将两个队列添加到集合中。 */
	xQueueAddToSet( xQueue1, xQueueSet );
	xQueueAddToSet( xQueue2, xQueueSet );

	/* 创建发送到队列的任务。 */
	xTaskCreate( vSenderTask1, "Sender1", 256, NULL, 1, NULL );
	xTaskCreate( vSenderTask2, "Sender2", 256, NULL, 1, NULL );

	/* 创建接收器任务。 */
	xTaskCreate( vReceiverTask, "Receiver", 256, NULL, 2, NULL );

	/* 启动调度程序，以便开始执行创建的任务。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

1.创建了2个队列，分别可容纳1个char指针。

2.创建1个队列集，可容纳2个队列。

3.把前面的2个队列添加到队列集中。

4.创建2个阻塞的发送任务，优先级都是1。创建1个非阻塞的接收任务，优先级是2。

然后启动OS执行任务。



```
static void vSenderTask1( void *pvParameters )
{
const TickType_t xBlockTime = pdMS_TO_TICKS( 100 );
const char * const pcMessage = "Message from vSenderTask1\r\n";

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 阻塞 100 毫秒。 */
		vTaskDelay( xBlockTime );

		/* 将此任务的字符串发送到 xQueue1。 即使队列只能容纳一项，也不必使用阻塞时间。 
		这是因为从队列中读取的任务的优先级高于本任务的优先级； 
		一旦这个任务写入队列，它就会被从队列中读取的任务抢占，
		所以到 xQueueSend() 的调用返回时，队列已经再次为空。 阻塞时间设置为 0。 */
		xQueueSend( xQueue1, &pcMessage, 0 );
	}
}
```

任务功能很简单，就是重发执行延时100毫秒，并把发送消息到队列。



```
static void vSenderTask2( void *pvParameters )
{
const TickType_t xBlockTime = pdMS_TO_TICKS( 200 );
const char * const pcMessage = "Message from vSenderTask2\r\n";

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 阻塞 200 毫秒。 */
		vTaskDelay( xBlockTime );

		/* 将此任务的字符串发送到 xQueue1。 即使队列只能容纳一项，也不必使用阻塞时间。 
		这是因为从队列中读取的任务的优先级高于本任务的优先级； 
		一旦这个任务写入队列，它就会被从队列中读取的任务抢占，
		所以到 xQueueSend() 的调用返回时，队列已经再次为空。 阻塞时间设置为 0。 */
		xQueueSend( xQueue2, &pcMessage, 0 );
	}
}
```

和上一个任务函数类似，只是延时长度为200毫秒。



```
static void vReceiverTask( void *pvParameters )
{
QueueHandle_t xQueueThatContainsData;
char *pcReceivedString;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 阻塞队列集以等待集合中的一个队列包含数据。 
		将从 xQueueSelectFromSet() 返回的 QueueSetMemberHandle_t 值转换为 QueueHandle_t，
		因为已知集合中的所有项目都是队列（与信号量相反，信号量也可以是队列集的成员）。 */
		xQueueThatContainsData = ( QueueHandle_t ) xQueueSelectFromSet( xQueueSet, portMAX_DELAY );

		/* 从集合中读取时使用了不确定的阻塞时间，因此 xQueueSelectFromSet() 不会返回，
		除非集合中的一个队列包含数据，并且 xQueueThatContansData 必须有效。 
		从队列中读取。 无需指定阻塞时间，因为已知队列包含数据。 阻塞时间设置为 0。 */
		xQueueReceive( xQueueThatContainsData, &pcReceivedString, 0 );

		/* 打印从队列中接收到的字符串。 */
		vPrintString( pcReceivedString );
	}
}
```

任务执行过程如下：

调用xQueueSelectFromSet()时发生阻塞。

当发送任务向队列xQueue1或xQueue2发送数据时，xQueueSelectFromSet()函数返回相应队列的句柄，接收任务退出阻塞状态。

调用xQueueReceive()，从对应的队列中读取消息。

然后将消息打印输出。



**另外举例说明**:

可以把包含不同类型数据的队列、信号量添加到队列集中。

接收任务需要在调用xQueueSelectFromSet()判断其返回值。

![1680963346847](image\1680963346847.png)

该例子队列集中，包含了2个队列和一个信号量。

由于他们的类型不同，只能通过xQueueSelectFromSet()的返回值，逐个比较以确定是哪个对象中有数据。



**知识点**：

1.队列集使用的场景是：当需要从多个队列/信号量中接收消息时，通过队列集来管理。只要一个队列/信号量有消息，就可让任务退出阻塞状态，读取相应消息。

2.注意：加入队列集的队列或信号量，不应该单独的读取。而是通过xQueueSelectFromSet()返回的句柄进行读取。



The End.

2023-04-08.