## FreeRTOS学习笔记[15]Example013-创建单次定时器和自动加载定时器(Lu)

本篇主要是学习如何使用软件定时器。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example013

把其中的main.c文件改为Example013.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example013分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example013/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example013

主要修改：

1.Example013.c中的main()函数名称改为 Example013()，然后在工程main.c文件中声明，并在初始化完成后调用 Example013()；

2.在FreeRTOSConfig.h中配置使能软件定时器：

```
/* Software timer related configuration options. */
#define configUSE_TIMERS						1
#define configTIMER_TASK_PRIORITY				( configMAX_PRIORITIES - 1 ) /* Maximum possible priority. */
#define configTIMER_QUEUE_LENGTH				2
#define configTIMER_TASK_STACK_DEPTH			( configMINIMAL_STACK_SIZE * 2 )
```



#### Step3.编译下载运行。

把Example013.c添加到工程，然后编译。

下载运行，会持续输出字符串。

![1680964561911](image\1680964561911.png)

#### Example013代码分析

```
/* 分别分配给一次性和自动重新加载计时器的周期。 */
#define mainONE_SHOT_TIMER_PERIOD		( pdMS_TO_TICKS( 3333UL ) )
#define mainAUTO_RELOAD_TIMER_PERIOD	( pdMS_TO_TICKS( 500UL ) )

/*-----------------------------------------------------------*/

/*一次性和自动重新加载计时器分别使用的回调函数。*/
static void prvOneShotTimerCallback( TimerHandle_t xTimer );
static void prvAutoReloadTimerCallback( TimerHandle_t xTimer );

/*-----------------------------------------------------------*/

int Example013( void )
{
TimerHandle_t xAutoReloadTimer, xOneShotTimer;
BaseType_t xTimer1Started, xTimer2Started;

	/* 创建一次性软件计时器，将创建的软件计时器的句柄存储在 xOneShotTimer 中。 */
	xOneShotTimer = xTimerCreate( "OneShot",	/* 软件计时器的文本名称 - FreeRTOS 不使用。*/
				  mainONE_SHOT_TIMER_PERIOD,	/* 软件计时器的周期（以滴答为单位）。 */
				  pdFALSE,	/* 将 uxAutoRealod 设置为 pdFALSE 会创建一个一次性软件计时器。 */
				  0,							/* 此示例不使用计时器 ID。 */
				  prvOneShotTimerCallback );	/* 正在创建的软件计时器要使用的回调函数。 */

	/* 创建自动重载软件定时器，将创建的软件定时器的句柄存储在 xAutoReloadTimer 中。 */
	xAutoReloadTimer = xTimerCreate( "AutoReload",/* 软件计时器的文本名称 - FreeRTOS不使用。*/
				 mainAUTO_RELOAD_TIMER_PERIOD,	/* 软件计时器的周期（以滴答为单位）。 */
				 pdTRUE,	/* 将 uxAutoRealod 设置为 pdTRUE 以创建自动重新加载软件计时器。 */
				 0,								/* 此示例不使用计时器 ID。 */
				 prvAutoReloadTimerCallback );	/* 正在创建的软件计时器要使用的回调函数。 */

	/* 检查计时器是否已创建。 */
	if( ( xOneShotTimer != NULL ) && ( xAutoReloadTimer != NULL ) )
	{
		/* 使用 0 块时间（无块时间）启动软件计时器。 
		调度程序尚未启动，因此此处指定的任何阻塞时间都将被忽略。 */
		xTimer1Started = xTimerStart( xOneShotTimer, 0 );
		xTimer2Started = xTimerStart( xAutoReloadTimer, 0 );

		/* xTimerStart() 的实现使用定时器命令队列，如果定时器命令队列已满，xTimerStart() 将失败。 
		在调度程序启动之前不会创建计时器服务任务，因此发送到命令队列的所有命令都将保留在队列中，
		直到调度程序启动后。 检查对 xTimerStart() 的两个调用均已通过。 */
		if( ( xTimer1Started == pdPASS ) && ( xTimer2Started == pdPASS ) )
		{
			/* 启动调度器。 */
			vTaskStartScheduler();
		}
	}

	/* 如果调度程序已启动，则永远不应到达以下行，
	因为 vTaskStartScheduler() 仅在没有足够的 FreeRTOS 堆内存可用于创建空闲和
	（如果已配置）定时器任务时才会返回。本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建1个单次定时器和1个自动加载定时器。

启动定时器，然后启动OS。



```
static void prvOneShotTimerCallback( TimerHandle_t xTimer )
{
static TickType_t xTimeNow;

	/* 获取当前滴答计数。 */
	xTimeNow = xTaskGetTickCount();

	/* 输出一个字符串以显示执行回调的时间。 */
	vPrintStringAndNumber( "One-shot timer callback executing", xTimeNow );
}
/*-----------------------------------------------------------*/

static void prvAutoReloadTimerCallback( TimerHandle_t xTimer )
{
static TickType_t xTimeNow;

	/* 获取当前滴答计数。 */
	xTimeNow = xTaskGetTickCount();

	/* 输出一个字符串以显示执行回调的时间。 */
	vPrintStringAndNumber( "Auto-reload timer callback executing", xTimeNow );
}
```

两个定时器任务功能都很简单，就是获取当前的滴答定时计数值，并打印输出。



**知识点**：

1.软件定时器，本质上是由一个守护任务管理定时时间，Step2.中配置优先级、任务栈等，实际上是这个守护任务的参数。

2.软件定时器，可以设置成一次定时，或周期性定时，由创建定时器时xTimerCreate()的第三个参数决定。

3.通过xTaskGetTickCount()可以获取当前滴答时钟计数值，这个值就是系统启动后运行的滴答时钟数。



The End.

2023-04-08.