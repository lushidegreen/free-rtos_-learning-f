## FreeRTOS学习笔记[16]Example014-使用回调函数参数，和软件定时器ID(Lu)

本篇主要是学习如何使用软件定时器回调函数参数，和软件定时器ID。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example014

把其中的main.c文件改为Example014.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example014分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example014/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example014

主要修改：

1.Example014.c中的main()函数名称改为 Example014()，然后在工程main.c文件中声明，并在初始化完成后调用 Example014()；

2.在FreeRTOSConfig.h中配置使能软件定时器：

```
/* Software timer related configuration options. */
#define configUSE_TIMERS						1
#define configTIMER_TASK_PRIORITY				( configMAX_PRIORITIES - 1 ) /* Maximum possible priority. */
#define configTIMER_QUEUE_LENGTH				2
#define configTIMER_TASK_STACK_DEPTH			( configMINIMAL_STACK_SIZE * 2 )
```



#### Step3.编译下载运行。

把Example014.c添加到工程，然后编译。

下载运行，会输出6个字符串，然后停止输出。

![1680965947925](image\1680965947925.png)

可以看出：周期定时器，运行5次后停止；单次定时器，运行1次后停止。

单次定时器，运行1次后停止。



#### Example014代码分析

```
/* 分别分配给一次性和自动重新加载计时器的周期。 */
#define mainONE_SHOT_TIMER_PERIOD		( pdMS_TO_TICKS( 3333UL ) )
#define mainAUTO_RELOAD_TIMER_PERIOD	( pdMS_TO_TICKS( 500UL ) )

/*-----------------------------------------------------------*/

/*
 * 两个计时器使用的回调函数。
 */
static void prvTimerCallback( TimerHandle_t xTimer );

/*-----------------------------------------------------------*/

/* 计时器句柄在回调函数内部使用，因此这次被赋予文件范围。 */
static TimerHandle_t xAutoReloadTimer, xOneShotTimer;

int Example014( void )
{
BaseType_t xTimer1Started, xTimer2Started;

	/* 创建一次性计时器，将创建的计时器的句柄存储在 xOneShotTimer 中。 */
	xOneShotTimer = xTimerCreate( "OneShot",	/* 计时器的文本名称 - FreeRTOS 不使用。*/
				  mainONE_SHOT_TIMER_PERIOD,	/* 计时器的周期（以滴答为单位）。 */
				  pdFALSE,	/* 将 uxAutoRealod 设置为 pdFALSE 以创建一次性计时器。 */
				  0,							/* 定时器 ID 初始化为 0。 */
				  prvTimerCallback );			/* 正在创建的计时器要使用的回调函数。 */

	/* 创建自动重新加载，将句柄存储到 xAutoReloadTimer 中创建的计时器。 */
	xAutoReloadTimer = xTimerCreate( "AutoReload",	/* 计时器的文本名称 - FreeRTOS不使用。*/
				 mainAUTO_RELOAD_TIMER_PERIOD,	/* 计时器的周期（以滴答为单位）。 */
				 pdTRUE,	/* 将 uxAutoRealod 设置为 pdTRUE 以创建自动重新加载计时器。 */
				 0,								/* 定时器 ID 初始化为 0。 */
				 prvTimerCallback );			/* 正在创建的计时器要使用的回调函数。 */

	/* 检查计时器是否已创建。 */
	if( ( xOneShotTimer != NULL ) && ( xAutoReloadTimer != NULL ) )
	{
		/* 使用 0 块时间（无块时间）启动计时器。 
		调度程序尚未启动，因此此处指定的任何阻塞时间都将被忽略。 */
		xTimer1Started = xTimerStart( xOneShotTimer, 0 );
		xTimer2Started = xTimerStart( xAutoReloadTimer, 0 );

		/* xTimerStart() 的实现使用定时器命令队列，如果定时器命令队列已满，xTimerStart() 将失败。 
		在调度程序启动之前不会创建计时器服务任务，因此发送到命令队列的所有命令都将保留在队列中，
		直到调度程序启动后。 检查对 xTimerStart() 的两个调用均已通过。 */
		if( ( xTimer1Started == pdPASS ) && ( xTimer2Started == pdPASS ) )
		{
			/* 启动调度器。 */
			vTaskStartScheduler();
		}
	}

	/* 如果调度程序已启动，则永远不应到达以下行，
	因为 vTaskStartScheduler() 仅在没有足够的 FreeRTOS 堆内存可用于创建空闲和
	（如果已配置）定时器任务时才会返回。本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建1个单次定时器和1个自动加载定时器，他们使用相同的回调函数，并且他们的初始ID都是0。

启动定时器，然后启动OS。



```
static void prvTimerCallback( TimerHandle_t xTimer )
{
TickType_t xTimeNow;
uint32_t ulExecutionCount;

	/* 该软件计时器已到期的次数计数存储在计时器的 ID 中。 
	获取 ID，将其递增，然后将其保存为新的 ID 值。 
	ID 是一个 void 指针，因此被强制转换为 uint32_t。 */
	ulExecutionCount = ( uint32_t ) pvTimerGetTimerID( xTimer );
	ulExecutionCount++;
	vTimerSetTimerID( xTimer, ( void * ) ulExecutionCount );

	/* 获取当前滴答计数。 */
	xTimeNow = xTaskGetTickCount();

    /* 创建计时器时，一次性计时器的句柄存储在 xOneShotTimer 中。 
	将传入此函数的句柄与 xOneShotTimer 进行比较，以确定是否是一次性或自动重新加载计时器过期，
	然后输出一个字符串以显示执行回调的时间。 */
	if( xTimer == xOneShotTimer )
	{
		vPrintStringAndNumber( "One-shot timer callback executing", xTimeNow );
	}
	else
	{
        /* xTimer 不等于 xOneShotTimer，因此它必须是过期的自动重新加载计时器。 */
		vPrintStringAndNumber( "Auto-reload timer callback executing", xTimeNow );

		if( ulExecutionCount == 5 )
		{
			/* 执行 5 次后停止自动重载计时器。 此回调函数在 RTOS 守护程序任务的上下文中执行，
			因此不得调用任何可能将守护程序任务置于阻塞状态的函数。 因此使用 0 的块时间。 */
			xTimerStop( xTimer, 0 );
		}
	}
}
```

回调函数的输入参数xTimer，就是对应软件定时器的句柄。

使用该句柄，调用pvTimerGetTimerID(xTimer)，即可获取定时器的ID。

对获取的ID加1，然后通过vTimerSetTimerID()，可以设置定时器的ID。

对于单次定时器xOneShotTimer，回调之后，就会自动停止。

而对于周期定时器xAutoReloadTimer()，当ID增加到5时，调用xTimerStop()强制停止。



**知识点**：

1.软件定时器回调函数的输入参数xTimer，就是对应软件定时器的句柄。

2.通过pvTimerGetTimerID()，即可获取定时器的ID。

3.通过vTimerSetTimerID()，可以修改定时器的ID。

4.通过xTimerStop()，可以强制停止周期定时器。

5.软件定时的ID，还可以用于多个定时器使用同一个回调函数的情景下。此时在回调函数中可以通过ID判断是哪个定时器触发的调用。



The End.

2023-04-08.