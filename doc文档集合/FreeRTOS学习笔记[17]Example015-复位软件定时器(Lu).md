## FreeRTOS学习笔记[17]Example015-复位软件定时器(Lu)

本篇主要是学习如何复位软件定时器。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example015

把其中的main.c文件改为Example015.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example015分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example015/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example015

主要修改：

1.Example015.c中的main()函数名称改为 Example015()，然后在工程main.c文件中声明，并在初始化完成后调用 Example015()；

2.在FreeRTOSConfig.h中配置使能软件定时器：

```
/* Software timer related configuration options. */
#define configUSE_TIMERS						1
#define configTIMER_TASK_PRIORITY				( configMAX_PRIORITIES - 1 ) /* Maximum possible priority. */
#define configTIMER_QUEUE_LENGTH				2
#define configTIMER_TASK_STACK_DEPTH			( configMINIMAL_STACK_SIZE * 2 )
```

3.添加串口中断回调函数：

```
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (USART1 == huart->Instance) {
		kbhit_flag = 1; // 用串口接收中断模拟按键按下
    	HAL_UART_Receive_IT(&huart1, uart_rxDat, 1); // 重新使能串口接收中断
	}
}
```

4.用kbhit_flag变量表示是否有按键动作：

```
		/* 是否按下了某个键？ */
		if( kbhit_flag != 0 ) // if( _kbhit() != 0 )
		{
			/* 按下按键后的处理... */
			
			/* 读取并丢弃被按下的键。 */
			kbhit_flag = 0; // ( void ) _getch();
		}
```



#### Step3.编译下载运行。

把Example015.c添加到工程，然后编译。

下载运行，会先输出字符串“Press a key to turn the backlight on.”。

5秒之后，输出“Timer expired, turning backlight OFF at time	5000”，表示背光自动关闭。

![1681009612983](image\1681009612983.png)

通过串口调试助手发送1个字节给MCU后，显示"... backlight ON ..."，表示背光打开。

5秒之后，显示"... backlight OFF ..."，表示背光关闭。

如果发送1字节给MCU后，5秒内再次发送1次，则显示“... resetting software ...”，背光关闭倒计时重新计算。



**这实际上，就是模拟手机等设备的背光控制过程**：

定时器时间到，触发回调函数，关闭背光。

有按键按下，如果背光处于熄灭状态，则点亮背光；如果原来是亮的，则保持点亮。复位定时器，进入倒计时。



#### Example015代码分析

```
/* 分配给一次性计时器的周期。 */
#define mainBACKLIGHT_TIMER_PERIOD		( pdMS_TO_TICKS( 5000UL ) )

/*-----------------------------------------------------------*/

/*
 * 定时器使用的回调函数。
 */
static void prvBacklightTimerCallback( TimerHandle_t xTimer );

/*
在真实目标上运行的真实应用程序可能会在中断中读取按钮按下。 
这允许应用程序是事件驱动的，并防止在没有按键被按下时轮询按键而浪费 CPU 时间。 
在使用 FreeRTOS Windows 端口时使用真正的中断是不切实际的，
因此创建了 vKeyHitTask() 任务以通过简单地轮询键盘来提供键读取功能。
 */
static void vKeyHitTask( void *pvParameters );

/*-----------------------------------------------------------*/

/* 这个例子没有真正的背光来打开和关闭，所以下面的变量只是用来保存背光的状态。 */
static BaseType_t xSimulatedBacklightOn = pdFALSE;

/* 用于关闭背光的软件定时器。 */
static TimerHandle_t xBacklightTimer = NULL;

extern UART_HandleTypeDef huart1; // 声明UART21句柄
static uint8_t uart_rxDat[10] = { 0 }; // 串口接收缓冲区

static uint8_t kbhit_flag = 0; // 按键按下标志

/*-----------------------------------------------------------*/

int Example015( void )
{
    /* 使能串口接收中断 */
    HAL_UART_Receive_IT(&huart1, uart_rxDat, 1); 

	/* 背光灯在开始时关闭。 */
	xSimulatedBacklightOn = pdFALSE;

	/* 创建一次性计时器，将创建的计时器的句柄存储在 xOneShotTimer 中。 */
	xBacklightTimer = xTimerCreate( "Backlight",/* 计时器的文本名称 - FreeRTOS 不使用。 */
					mainBACKLIGHT_TIMER_PERIOD,	/* 计时器的周期（以滴答为单位）。 */
					pdFALSE,	/* 将 uxAutoRealod 设置为 pdFALSE 以创建一次性计时器。 */
					0,			/* The timer ID is not used in this example. */
					prvBacklightTimerCallback );/* 正在创建的计时器要使用的回调函数。 */

	/*在真实目标上运行的真实应用程序可能会在中断中读取按钮按下。 
	这允许应用程序是事件驱动的，并防止在没有按键被按下时轮询按键而浪费 CPU 时间。 
	在使用 FreeRTOS Windows 端口时使用真正的中断是不切实际的，
	因此创建了 vKeyHitTask() 任务来通过简单地轮询键盘来提供键读取功能。 */
	xTaskCreate( vKeyHitTask, "Key poll", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );

	/* 启动定时器 */
	xTimerStart( xBacklightTimer, 0 );

	/* 启动调度器。 */
	vTaskStartScheduler();

	/* 与前面的示例一样，vTaskStartScheduler() 不应返回，因此不应到达以下行。 */
	for( ;; );
	return 0;
}
```

创建1个单次定时器。

创建1个任务用于扫描按键输入。

启动定时器，然后启动OS。



```
static void prvBacklightTimerCallback( TimerHandle_t xTimer )
{
TickType_t xTimeNow = xTaskGetTickCount();

	/* 背光定时器超时，关闭背光。 */
	xSimulatedBacklightOn = pdFALSE;

	/* 打印背光关闭的时间。 */
	vPrintStringAndNumber( "Timer expired, turning backlight OFF at time\t", xTimeNow );
}
```

功能很简单，就是把模拟背光开关的变量设为pdFALSE，然后打印“backlight OFF”。



```
static void vKeyHitTask( void *pvParameters )
{
const TickType_t xShortDelay = pdMS_TO_TICKS( 50 );
TickType_t xTimeNow;

	vPrintString( "Press a key to turn the backlight on.\r\n" );

	/* 在真实目标上运行的真实应用程序可能会在中断中读取按钮按下。 
	这允许应用程序是事件驱动的，并防止在没有按键被按下时轮询按键而浪费 CPU 时间。 
	在使用 FreeRTOS Windows 端口时使用真正的中断是不切实际的，
	因此创建此任务是为了通过简单地轮询键盘来提供键读取功能。 */
	for( ;; )
	{
		/* 是否按下了某个键？ */
		if( kbhit_flag != 0 ) // if( _kbhit() != 0 )
		{
			/* 记录按下按键的时间。 */
			xTimeNow = xTaskGetTickCount();

			/* 一个键被按下。 */
			if( xSimulatedBacklightOn == pdFALSE )
			{
				/* 背光已关闭，因此将其打开并打印打开时间。 */
				xSimulatedBacklightOn = pdTRUE;
				vPrintStringAndNumber( "Key pressed, turning backlight ON at time\t", xTimeNow );
			}
			else
			{
				/* 背光已经打开，因此打印一条消息，说明背光即将重置以及重置的时间。 */
				vPrintStringAndNumber( "Key pressed, resetting software timer at time\t", xTimeNow );
			}

			/* 重置软件定时器。 如果背光之前关闭，此调用将启动计时器。 
			如果背光以前在此调用上，将重新启动计时器。 一个真正的应用程序可能会在中断中读取按键。 
			如果此函数是中断服务程序，则必须使用 xTimerResetFromISR() 而不是 xTimerReset()。 */
			xTimerReset( xBacklightTimer, xShortDelay );

			/* 读取并丢弃被按下的键。 */
			kbhit_flag = 0; // ( void ) _getch();
		}

		/* 不要轮询太快。 */
		vTaskDelay( xShortDelay );
	}
}
```

检测到按键输入时，如果背光标记为OFF，则把标记改为ON，并打印“backlight ON”；

否则，如果背光标记已经为ON，则打印“resetting software timer”。

每次按键按下，都调用xTimerReset()复位定时器。



```
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (USART1 == huart->Instance) {
		kbhit_flag = 1; // 用串口接收中断模拟按键按下
    	HAL_UART_Receive_IT(&huart1, uart_rxDat, 1); // 重新使能串口接收中断
	}
}
```

void HAL_UART_RxCpltCallback()，是STM32的HAL库指定的串口接收完成回调函数。

在使用HAL库编写的UART程序中，调用HAL_UART_Receive_IT()使能中断之后，接收完成才会调用回调函数。

而且，在接收完成中断中，必须调用HAL_UART_Receive_IT()重新使能中断，下次接收才会调用回调函数。



**知识点**：

一次性软件定时器停止后，可以通过xTimerReset()重启定时器。



The End.

2023-04-09.