## FreeRTOS学习笔记[18]Example016-使用二值信号量实现任务和中断的同步(Lu)

本篇主要是学习使用二值信号量实现任务和中断的同步。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example016

把其中的main.c文件改为Example016.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example016分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example016/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example016

主要修改：

1.Example016.c中的main()函数名称改为 Example016()，然后在工程main.c文件中声明，并在初始化完成后调用 Example016()；

2.Example016.c中删除用于Windows环境下模拟中断的vPeriodicTask()任务和vPortSetInterruptHandler()配置。

本例程在STM32平台下，使用硬件TIM2定时中断来替代。

3.在Example016()中，调用vTaskStartScheduler()启动调度器前，启动TIM1并使能中断：

```
		/* 启动TIM1并使能中断 */
		HAL_TIM_Base_Start_IT(&htim2);
```

4.在工程main.c文件中声明ulExampleInterruptHandler()，并在TIM中断回调函数中调用该函数。

```
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  // ......
  if (htim->Instance == TIM2) {
    ulExampleInterruptHandler();
  }
  // ......
}
```



#### Step3.编译下载运行。

把Example016.c添加到工程，然后编译。

下载运行，每隔1秒钟输出字符串“Handler task - Processing event.”。

![1681022996509](image\1681022996509.png)



#### Example016代码分析

```
/* 要创建的任务。 */
static void vHandlerTask( void *pvParameters );

extern TIM_HandleTypeDef htim2;

/*-----------------------------------------------------------*/

/* 声明一个 SemaphoreHandle_t 类型的变量。 这用于引用用于将任务与中断同步的信号量。 */
SemaphoreHandle_t xBinarySemaphore;

int Example016( void )
{
    /* 在使用信号量之前，必须显式创建它。 在此示例中，创建了一个二进制信号量。 */
    xBinarySemaphore = xSemaphoreCreateBinary();

	/* 检查信号量是否已成功创建。 */
	if( xBinarySemaphore != NULL )
	{
		/* 创建“处理程序”任务，这是中断处理被推迟到的任务，将与中断同步的任务也是如此。 
		处理程序任务以高优先级创建，以确保它在中断退出后立即运行。 在这种情况下，选择优先级 3。 */
		xTaskCreate( vHandlerTask, "Handler", 1000, NULL, 3, NULL );
		/* 启动TIM1并使能中断 */
		HAL_TIM_Base_Start_IT(&htim2);
		/* 启动调度程序，以便开始执行创建的任务。 */
		vTaskStartScheduler();
	}

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建1个二值信号量。

创建1个任务用于接收信号量并打印信息。

启动TIM1并使能中断，然后启动OS。

然后启动OS。



```
static void vHandlerTask( void *pvParameters )
{
	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 使用信号量等待事件。 信号量是在调度程序启动之前创建的，因此在此任务第一次运行之前。 
		任务无限期地阻塞意味着这个函数调用只会在成功获取信号量后返回——因此不需要检查返回值。 */
		xSemaphoreTake( xBinarySemaphore, portMAX_DELAY );

		/* 要到达这里，事件必须已经发生。 处理事件（在这种情况下，只需打印一条消息）。 */
		vPrintString( "Handler task - Processing event.\r\n" );
	}
}
```

功能很简单，就是等待信号量，然后打印一条信息。



```
void ulExampleInterruptHandler( void )
{
BaseType_t xHigherPriorityTaskWoken;

	/* xHigherPriorityTaskWoken 参数必须初始化为 pdFALSE，
	因为如果需要上下文切换，它将在中断安全 API 函数内设置为 pdTRUE。 */
	xHigherPriorityTaskWoken = pdFALSE;

	/* “给”信号量以解除阻塞任务。 */
	xSemaphoreGiveFromISR( xBinarySemaphore, &xHigherPriorityTaskWoken );

	/* 将 xHigherPriorityTaskWoken 值传递给 portYIELD_FROM_ISR()。 
	如果在 xSemaphoreGiveFromISR() 中将 xHigherPriorityTaskWoken 设置为 pdTRUE，
	则调用 portYIELD_FROM_ISR() 将请求上下文切换。 
	如果 xHigherPriorityTaskWoken 仍为 pdFALSE，则调用 portYIELD_FROM_ISR() 将无效。 
	Windows 端口使用的 portYIELD_FROM_ISR() 的实现包括一个返回语句，这就是该函数没有显式返回值的原因。 */
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
```

发送信号量给任务。



```
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
  if (htim->Instance == TIM2) {
    ulExampleInterruptHandler();
  }
  /* USER CODE END Callback 1 */
}
```

void HAL_TIM_PeriodElapsedCallback()，是STM32的HAL库指定的所有定时器周期中断回调函数。在该函数中，通过htim->Instance判断当前中断所对应的定时器。

在前面构建基础工程时，把HAL时基改成了TIM1，所以这里调用了HAL_IncTick()，以实现HAL的各种超时机制。

在使用HAL库编写的TIM程序中，调用HAL_TIM_Base_Start_IT()使能中断之后，定时时间到才会调用回调函数。



**知识点**：

1.创建二值信号量使用宏xSemaphoreCreateBinary()，实际调用的是xQueueGenericCreate()函数。

2.等待信号量使用宏xSemaphoreTake()，实际调用的是xQueueSemaphoreTake()函数。

3.在中断中发送信号量使用xSemaphoreGiveFromISR()，在任务中发送信号量使用xSemaphoreGive()。

4.在中断中调用xSemaphoreGiveFromISR()后，把第二个参数输出值传给portYIELD_FROM_ISR()。这是因为发送信号量后，可能会激活等待的任务，在退出后就需要切换上下文。



The End.

2023-04-09.