## FreeRTOS学习笔记[20]Example018-集中延迟中断处理(Lu)

本篇主要是学习利用RTOS的守护任务，把中断事件集中延迟处理。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example018

把其中的main.c文件改为Example018.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example018分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example018/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example018

主要修改：

1.Example018.c中的main()函数名称改为 Example018()，然后在工程main.c文件中声明，并在初始化完成后调用 Example018()；

2.在FreeRTOSConfig.h中配置使能软件定时器：

```
/* Software timer related configuration options. */
#define configUSE_TIMERS						1
#define configTIMER_TASK_PRIORITY				( configMAX_PRIORITIES - 1 ) /* Maximum possible priority. */
#define configTIMER_QUEUE_LENGTH				2
#define configTIMER_TASK_STACK_DEPTH			( configMINIMAL_STACK_SIZE * 2 )
```

3.添加串口中断回调函数，并在该函数中调用ulExampleInterruptHandler()：

```
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if (USART1 == huart->Instance) {
        ulExampleInterruptHandler();
        HAL_UART_Receive_IT(&huart1, uart_rxDat, 1); // 重新使能串口接收中断
    }
}
```



#### Step3.编译下载运行。

把Example018.c添加到工程，然后编译。

下载运行，通过串口调试助手发送1个字节给MCU后，MCU输出字符串“Handler function - Processing event”。

![1681052384996](image\1681052384996.png)

程序运行过程是：MCU串口接收到一个字符，并产生一次中断；在中断函数中，发送事件处理函数指针给RTOS守护任务，然后守护任务打印出字符串。



#### Example018代码分析

```
/* 执行延迟中断处理的函数。 此函数在守护程序任务的上下文中执行。 */
static void vDeferredHandlingFunction( void *pvParameter1, uint32_t ulParameter2 );

extern UART_HandleTypeDef huart1; // 声明UART1句柄
static uint8_t uart_rxDat[10] = { 0 }; // 串口接收缓冲区

/*-----------------------------------------------------------*/

int Example018( void )
{
    /* 使能串口接收中断 */
    HAL_UART_Receive_IT(&huart1, uart_rxDat, 1); 

	/* 启动调度程序，以便创建的任务开始执行。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

使能串口接收中断，然后启动OS。



```
static void ulExampleInterruptHandler( void )
{
static uint32_t ulParameterValue = 0;
BaseType_t xHigherPriorityTaskWoken;

	/* xHigherPriorityTaskWoken 参数必须初始化为 pdFALSE，
	因为如果需要上下文切换，它将在中断安全 API 函数内设置为 pdTRUE。 */
	xHigherPriorityTaskWoken = pdFALSE;

	/* 将指向中断延迟处理函数的指针发送给守护程序任务。 
	未使用延迟处理函数的 pvParameter1 参数，因此只需设置为 NULL。 
	延迟处理函数的 ulParameter2 参数用于传递一个数字，该数字在每次发生此中断时加一。 */
	xTimerPendFunctionCallFromISR( vDeferredHandlingFunction, NULL, ulParameterValue, &xHigherPriorityTaskWoken );
	ulParameterValue++;

	/* 将 xHigherPriorityTaskWoken 值传递给 portYIELD_FROM_ISR()。 
	如果在 xTimerPendFunctionCallFromISR() 中将 xHigherPriorityTaskWoken 设置为 pdTRUE，
	则调用 portYIELD_FROM_ISR() 将请求上下文切换。 
	如果 xHigherPriorityTaskWoken 仍为 pdFALSE，则调用 portYIELD_FROM_ISR() 将无效。 
	Windows 端口使用的 portYIELD_FROM_ISR() 的实现包括一个返回语句，这就是该函数没有显式返回值的原因。 */
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
```

这是中断处理函数，发送事件处理函数指针给RTOS守护任务。本例中，在串口接收中断中被调用。



```
static void vDeferredHandlingFunction( void *pvParameter1, uint32_t ulParameter2 )
{
	/* 删除编译器警告，指出未使用 pvParameter1，因为此示例中未使用 pvParameter1。 */
	( void ) pvParameter1;

	/* 处理事件 - 在这种情况下，只需打印一条消息和 ulParameter2 的值。 */
	vPrintStringAndNumber( "Handler function - Processing event ", ulParameter2 );
}
```

这是延迟处理函数，功能很简单，就是打印出字符串，以及从中断传递过来的参数。



```
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if (USART1 == huart->Instance) {
        ulExampleInterruptHandler();
        HAL_UART_Receive_IT(&huart1, uart_rxDat, 1); // 重新使能串口接收中断
    }
}
```

void HAL_UART_RxCpltCallback()，是STM32的HAL库指定的串口接收完成回调函数。

在使用HAL库编写的UART程序中，调用HAL_UART_Receive_IT()使能中断之后，接收完成才会调用回调函数。

而且，在接收完成中断中，必须调用HAL_UART_Receive_IT()重新使能中断，下次接收才会调用回调函数。



**知识点**：

1.守护任务是一个标准的FreeRTOS任务，配置宏configUSE_TIMERS为1，使能软件定时器功能时，它就会在调度程序启动时自动创建。

2.函数xTimerPendFunctionCall()或xTimerPendFunctionCallFromISR()的功能，是发送一个“处理函数”指针给守护任务。当守护任务运行时，就再执行“处理函数”。

3.守护任务优先级通过宏configTIMER_TASK_PRIORITY配置，队列长度由宏configTIMER_QUEUE_LENGTH配置。



**特别注意**：

使用xTimerPendFunctionCall()、xTimerPendFunctionCallFromISR()时，要注意调整软定时器的队列长度，避免队列过短而导致发送失败。特别是守护任务优先级较低时，或者通过中断函数发送“处理函数”时。



The End.

2023-04-09.