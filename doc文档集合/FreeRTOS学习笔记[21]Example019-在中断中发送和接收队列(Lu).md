## FreeRTOS学习笔记[21]Example019-在中断中发送和接收队列(Lu)

本篇主要是学习在中断中发送和接收队列。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example019

把其中的main.c文件改为Example019.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example019分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example019/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example019

主要修改：

1.Example019.c中的main()函数名称改为 Example019()，然后在工程main.c文件中声明，并在初始化完成后调用 Example019()；

2.Example019.c中删除用于Windows环境下模拟中断的vPortSetInterruptHandler()配置。本例程在STM32平台下，使用硬件TIM2定时中断来替代。

3.任务栈大小从1000改为256，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

4.在工程main.c文件中声明ulExampleInterruptHandler()，并在TIM中断回调函数中调用该函数。

```
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  // ......
  if (htim->Instance == TIM2) {
    ulExampleInterruptHandler();
  }
  // ......
}
```

5.在任务函数vIntegerGenerator()中，设置TIM2的计数值，然后启动TIM2并使能中断：(相当于之前的触发中断)

```
		/* 设置TIM1的计数值CNT */
		__HAL_TIM_SET_COUNTER(&htim2, 1);
		/* 启动TIM1并使能中断 */
		HAL_TIM_Base_Start_IT(&htim2);
```

6.在中断处理函数ulExampleInterruptHandler()中，停止TIM2并禁止中断：

```
	/* 停止TIM1并禁止中断 */
	HAL_TIM_Base_Stop_IT(&htim2);
```



#### Step3.编译下载运行。

把Example019.c添加到工程，然后编译。

下载运行，会持续输出字符串“String ...”。

![1681303549430](image\1681303549430.png)

可以把vIntegerGenerator()中的

```
vTaskDelayUntil( &xLastExecutionTime, xDelay200ms );
```

改为

```
vTaskDelayUntil( &xLastExecutionTime, xDelay200ms * 5 );
```

延长中断的间隔，就可以观察到，字符串是以5行为一组输出的。



#### Example019代码分析

```
/* 要创建的任务。 */
static void vIntegerGenerator( void *pvParameters );
static void vStringPrinter( void *pvParameters );

extern TIM_HandleTypeDef htim2;

/*-----------------------------------------------------------*/

/* 声明 QueueHandle_t 类型的两个变量。 一个队列将从 ISR 中读取，另一个队列将从 ISR 中写入。 */
QueueHandle_t xIntegerQueue, xStringQueue;

int Example019( void )
{
    /* 在使用队列之前，必须先创建它。 创建此示例使用的两个队列。 
	一个队列可以保存 uint32_t 类型的变量，另一个队列可以保存 char* 类型的变量。 
	两个队列最多可容纳 10 个项目。 实际应用程序应检查返回值以确保已成功创建队列。 */
    xIntegerQueue = xQueueCreate( 10, sizeof( uint32_t ) );
	xStringQueue = xQueueCreate( 10, sizeof( char * ) );

	/* 创建使用队列将整数传递给中断服务例程的任务。 任务以优先级 1 创建。 */
	xTaskCreate( vIntegerGenerator, "IntGen", 256, NULL, 1, NULL );

	/* 创建打印出从中断服务例程发送给它的字符串的任务。 该任务以较高的优先级 2 创建。 */
	xTaskCreate( vStringPrinter, "String", 256, NULL, 2, NULL );

	/* 启动调度程序，以便开始执行创建的任务。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建两个队列。

创建两个任务。

然后启动OS。



```
static void vIntegerGenerator( void *pvParameters )
{
TickType_t xLastExecutionTime;
const TickType_t xDelay200ms = pdMS_TO_TICKS( 200UL ), xDontBlock = 0;
uint32_t ulValueToSend = 0;
BaseType_t i;

	/* 初始化调用 vTaskDelayUntil() 使用的变量。 */
	xLastExecutionTime = xTaskGetTickCount();

	for( ;; )
	{
		/* 这是一个周期性的任务。 阻塞直到再次运行。 该任务将每 1000 毫秒执行一次。 */
		vTaskDelayUntil( &xLastExecutionTime, xDelay200ms * 5 );

		/* 将五个数字发送到队列，每个值都比前一个值高一个。 这些数字由中断服务程序从队列中读取。 
		中断服务程序总是清空队列，所以这个任务保证能够写入所有五个值，而不需要指定一个阻塞时间。 */
		for( i = 0; i < 5; i++ )
		{
			xQueueSendToBack( xIntegerQueue, &ulValueToSend, xDontBlock );
			ulValueToSend++;
		}

		/* 设置TIM1的计数值CNT */
		__HAL_TIM_SET_COUNTER(&htim2, 1);
		/* 启动TIM1并使能中断 */
		HAL_TIM_Base_Start_IT(&htim2);
	}
}
```

vIntegerGenerator()任务的功能，是定时(1000ms)发送5次消息到队列xIntegerQueue，并触发一次中断。

先把TIM2计数值设为1，然后启动TIM2并使能中断。因为创建基础工程时TIM2设置的计数方向是DOWN，当计数值减到0并溢出时，就会触发中断。



```
void ulExampleInterruptHandler( void )
{
BaseType_t xHigherPriorityTaskWoken;
uint32_t ulReceivedNumber;

/* 这些字符串被声明为 static const 以确保它们没有分配到中断服务程序的堆栈上，并且即使在中断服务程序没有执行时也存在。 */
static const char *pcStrings[] =
{
	"String 0\r\n",
	"String 1\r\n",
	"String 2\r\n",
	"String 3\r\n"
};

	/* 与往常一样，xHigherPriorityTaskWoken 被初始化为 pdFALSE，
	以便能够检测到它在中断安全 API 函数中被设置为 pdTRUE。 */
	xHigherPriorityTaskWoken = pdFALSE;

	/* 从队列中读取，直到队列为空。 */
	while( xQueueReceiveFromISR( xIntegerQueue, &ulReceivedNumber, &xHigherPriorityTaskWoken ) != errQUEUE_EMPTY )
	{
		/* 将接收到的值截断为最后两位（值 0 到 3 .），
		然后使用截断的值作为 pcStrings[] 数组的索引来选择要在另一个队列上发送的字符串 (char *)。 */
		ulReceivedNumber &= 0x03;
		xQueueSendToBackFromISR( xStringQueue, &pcStrings[ ulReceivedNumber ], &xHigherPriorityTaskWoken );
	}

    /* 如果从 xIntegerQueue 接收导致任务离开 Blocked 状态，
	并且离开 Blocked 状态的任务的优先级高于处于 Running 状态的任务的优先级，
	则 xHigherPriorityTaskWoken 将在 xQueueReceiveFromISR() 中设置为 pdTRUE .

    如果发送到 xStringQueue 导致任务离开 Blocked 状态，
	并且如果离开 Blocked 状态的任务的优先级高于处于 Running 状态的任务的优先级，
	则 xHigherPriorityTaskWoken 将在 xQueueSendFromISR() 中设置为 pdTRUE .

    xHigherPriorityTaskWoken 用作 portYIELD_FROM_ISR() 的参数。 
	如果 xHigherPriorityTaskWoken 等于 pdTRUE，则调用 portYIELD_FROM_ISR() 将请求上下文切换。 
	如果 xHigherPriorityTaskWoken 仍为 pdFALSE，则调用 portYIELD_FROM_ISR() 将无效。

    Windows端口使用的portYIELD_FROM_ISR()的实现包括一个返回语句,这就是该函数没有显式返回值的原因。*/
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );

	/* 停止TIM2并禁止中断 */
	HAL_TIM_Base_Stop_IT(&htim2);
}
```

该函数在TIM2中断中调用。

功能是从队列xIntegerQueue读取消息，并根据收到的消息，发送消息到另一个队列xStringQueue。发送的消息内容，是const char *pcStrings[]数组中某个字符串的地址。



```
static void vStringPrinter( void *pvParameters )
{
char *pcString;

	for( ;; )
	{
		/* 阻塞队列以等待数据到达。 */
		xQueueReceive( xStringQueue, &pcString, portMAX_DELAY );

		/* 打印出接收到的字符串。 */
		vPrintString( pcString );
	}
}
```

vStringPrinter()任务的功能，是从队列xStringQueue读取消息，然后把对应的字符串打印出来。



```
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
  if (htim->Instance == TIM2) {
    ulExampleInterruptHandler();
  }
  /* USER CODE END Callback 1 */
}
```

void HAL_TIM_PeriodElapsedCallback()，是STM32的HAL库指定的所有定时器周期中断回调函数。在该函数中，通过htim->Instance判断当前中断所对应的定时器。

在前面构建基础工程时，把HAL时基改成了TIM1，所以这里调用了HAL_IncTick()，以实现HAL的各种超时机制。

在使用HAL库编写的TIM程序中，调用HAL_TIM_Base_Start_IT()使能中断之后，定时时间到才会调用回调函数。



**知识点**：

1.在中断中接收队列消息，用xQueueReceiveFromISR()，该函数没有超时参数，不管是否成功，都会立即返回。

2.在中断中发送队列消息，用xQueueSendToBackFromISR()发送到队尾，或xQueueSendToFrontFromISR()发送到队列头。



The End.

2023-04-12.