## FreeRTOS学习笔记[22]Example020-用信号量重写vPrintString()函数(Lu)

本篇主要是学习使用互斥信号量来控制对资源的使用权限。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example020

把其中的main.c文件改为Example020.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example020分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example020/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example020

主要修改：

1.Example020.c中的main()函数名称改为 Example020()，然后在工程main.c文件中声明，并在初始化完成后调用 Example020()；

2.任务栈大小从1000改为512，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

3.在FreeRTOSConfig.h中配置使能互斥信号量：

```
#define configUSE_MUTEXES           1
```

4.删除_kbhit()和vTaskEndScheduler()调用代码，本例中不需要演示通过按键停止调度器。



#### Step3.编译下载运行。

把Example020.c添加到工程，然后编译。

下载运行，会持续输出字符串：

![1681305717645](image\1681305717645.png)

Task 1和Task 2分别输出不同的字符串。



#### Example020代码分析

```
/* 要创建的任务。 将创建此任务的两个实例。 */
static void prvPrintTask( void *pvParameters );

/* 使用互斥体控制对标准输出的访问的函数。 */
static void prvNewPrintString( const char *pcString );

extern UART_HandleTypeDef huart1; // 声明UART1句柄

/*-----------------------------------------------------------*/

/* 声明一个 SemaphoreHandle_t 类型的变量。 这用于引用互斥类型信号量，用于确保对标准输出的互斥访问。 */
SemaphoreHandle_t xMutex;

/* 任务阻塞在 0 到 xMaxBlockTime 滴答之间的伪随机时间。 */
const TickType_t xMaxBlockTimeTicks = 0x20;

int Example020( void )
{
    /* 在使用信号量之前，必须显式创建它。 在本例中，创建了一个互斥类型的信号量。 */
    xMutex = xSemaphoreCreateMutex();

	/* 检查信号量是否已成功创建。 */
	if( xMutex != NULL )
	{
		/* 创建两个尝试写入标准输出的任务实例。 
		他们尝试写入的字符串作为任务的参数传递到任务中。 
		这些任务以不同的优先级创建，因此会发生一些抢占。 */
		xTaskCreate( prvPrintTask, "Print1", 512, 
					"Task 1 ******************************************\r\n", 1, NULL );
		xTaskCreate( prvPrintTask, "Print2", 512, 
					"Task 2 ------------------------------------------\r\n", 2, NULL );

		/* 启动调度程序，以便开始执行创建的任务。 */
		vTaskStartScheduler();
	}

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建一个互斥量，创建两个任务。然后启动OS。



```
static void prvNewPrintString( const char *pcString )
{
	/* 信号量是在调度程序启动之前创建的，因此在该任务执行时已经存在。

	尝试获取信号量，如果互斥锁不能立即使用，则无限期阻塞。 
	对 xSemaphoreTake() 的调用只会在成功获取信号量时返回，因此无需检查返回值。 
	如果使用了任何其他延迟时间，则代码必须在访问资源之前检查 xSemaphoreTake() 是否返回 pdTRUE（在本例中为标准输出。 */
	xSemaphoreTake( xMutex, portMAX_DELAY );
	{
		/* 只有在成功获得信号量后才会执行以下行 - 因此可以自由访问标准输出。 */
		printf( "%s", pcString );
		fflush( stdout );
	}
	xSemaphoreGive( xMutex );
}
```

这是重写后的vPrintString()，通过互斥量实现了不同任务对标准输出printf()的互斥访问管理。



```
static void prvPrintTask( void *pvParameters )
{
char *pcStringToPrint;
const TickType_t xSlowDownDelay = pdMS_TO_TICKS( 5UL );

	/* 将创建此任务的两个实例。 任务打印的字符串使用任务的参数传递给任务。 参数被强制转换为所需的类型。 */
	pcStringToPrint = ( char * ) pvParameters;

	for( ;; )
	{
		/* 使用新定义的函数打印出字符串。 */
		prvNewPrintString( pcStringToPrint );

		/* 等待一个伪随机时间。 
		请注意， rand() 不一定是可重入的，但在这种情况下，它并不重要，因为代码并不关心返回什么值。 
		在更安全的应用程序中，应使用已知可重入的rand()版本，或者应使用临界区保护对rand()的调用。 */
		vTaskDelay( rand() % xMaxBlockTimeTicks );

		/* 只是为了确保滚动不会太快！ */
		vTaskDelay( xSlowDownDelay );
	}
}
```

任务函数功能很简单：调用prvNewPrintString()打印字符串，并根据rand()函数返回值，进行随机延时。



**测试**：

因为两个任务都调用prvNewPrintString()打印字符串，就有可能出现冲突。在没有使用互斥量时，当一个任务(Task 1)正在打印时，被另一个更高优先级的任务(Task 2)抢占，导致字符串不完整。

上面prvNewPrintString()的实现代码，即使屏蔽了互斥信号量的使用：

```
xSemaphoreTake( xMutex, portMAX_DELAY );
xSemaphoreGive( xMutex );
```

也没法演示出发生冲突的情况。

为了演示冲突的情况，修改prvNewPrintString()的实现代码：

```
static void uart_send_string(const char *str)
{
    uint16_t len;
    len = strlen(str);
    while (len--) {
        HAL_UART_Transmit(&huart1, (uint8_t *)str++, 1, 0xFFFF);
        HAL_Delay(1); // 增加延时, 目的是制造两个任务对UART的访问冲突
    }
    
}
// 通过uart_send_string()实现的字符串输出函数
static void prvNewPrintString( const char *pcString )
{
	// xSemaphoreTake( xMutex, portMAX_DELAY );
	{
		uart_send_string( pcString );
	}
	// xSemaphoreGive( xMutex );
}
```

把printf()替换为uart_send_string()。uart_send_string()故意增加延时， 制造两个任务对UART的访问冲突。

屏蔽xSemaphoreTake()和xSemaphoreGive()后，编译下载运行，输出如下：

![1681307682824](image\1681307682824.png)

从上图还可以看出，被打断的只有Task 1，而Task 2总能打印出完整的字符串。因为Task 2的优先级比Task 1高。

取消屏蔽xSemaphoreTake()和xSemaphoreGive()后，则Task 1和Task 2都能完整地打印字符串，和最前面的图片一样。这就体现了互斥量对资源访问的保护。



**知识点**：

1.创建互斥信号量，使用xSemaphoreCreateMutex()。

2.等待、获取互斥信号量，使用xSemaphoreTake()。

3.释放互斥信号量，使用xSemaphoreGive()。

4.必须配置configUSE_MUTEXES为1，才能使用互斥信号量。



The End.

2023-04-12.