## FreeRTOS学习笔记[23]Example021-用看守任务重写vPrintString函数(Lu)

本篇主要是学习使用看守任务来管理一个资源。

**概述**：

本例程提供了vPrintString()的另一种实现方式。使用一个看守任务来管理对标准输出的访问。当一个任务想要将消息写入标准输出时，它不直接调用printf函数，而是将消息发送给看守任务。

本例中，看守任务是唯一允许访问标准输出的任务，所以不需要考虑互斥的问题。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example021

把其中的main.c文件改为Example021.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example021分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example021/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example021

主要修改：

1.Example021.c中的main()函数名称改为 Example021()，然后在工程main.c文件中声明，并在初始化完成后调用 Example021()；

2.任务栈大小从1000改为256，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

3.在FreeRTOSConfig.h中配置使能TICK钩子函数：(本例中使用它来发送消息)

```
#define configUSE_TICK_HOOK			1
```

并使能申请内存失败钩子函数：(本例中只是作为一个扩展知识)

```
#define configUSE_MALLOC_FAILED_HOOK		1
```



#### Step3.编译下载运行。

把Example021.c添加到工程，然后编译。

下载运行，会持续输出字符串：

![1681309073077](image\1681309073077.png)

TICK钩子函数、Task 1和Task 2分别输出不同的字符串。

实际上，这些字符串并不是由TICK钩子函数、Task 1和Task 2直接发送的，而是由看守任务prvStdioGatekeeperTask()发送的。



#### Example021代码分析

```
/* 向 stdio 看守发送消息的任务。 将创建此任务的两个实例。 */
static void prvPrintTask( void *pvParameters );

/* 看守任务。 */
static void prvStdioGatekeeperTask( void *pvParameters );

/* 定义任务和中断将通过看守任务打印出的字符串。 */
static const char *pcStringsToPrint[] =
{
	"Task 1 ****************************************************\r\n",
	"Task 2 ----------------------------------------------------\r\n",
	"Message printed from the tick hook interrupt ##############\r\n"
};

/*-----------------------------------------------------------*/

/* 声明一个 QueueHandle_t 类型的变量。 这用于将消息从打印任务发送到看守任务。 */
static QueueHandle_t xPrintQueue;

/* 任务阻塞在 0 到 xMaxBlockTime 滴答之间的伪随机时间。 */
const TickType_t xMaxBlockTimeTicks = 0x20;

int Example021( void )
{
    /* 在使用队列之前，必须显式创建它。 创建队列时最多可容纳 5 个字符指针。 */
    xPrintQueue = xQueueCreate( 5, sizeof( char * ) );

	/* 检查队列是否创建成功。 */
	if( xPrintQueue != NULL )
	{
		/* 创建两个向看守任务发送消息的任务实例。 
		他们尝试写入的字符串的索引作为任务参数（xTaskCreate() 的第四个参数）传入。 
		这些任务以不同的优先级创建，因此会发生一些抢占。 */
		xTaskCreate( prvPrintTask, "Print1", 256, ( void * ) 0, 1, NULL );
		xTaskCreate( prvPrintTask, "Print2", 256, ( void * ) 1, 2, NULL );

		/* 创建看守任务。 这是唯一允许访问标准输出的任务。 */
		xTaskCreate( prvStdioGatekeeperTask, "Gatekeeper", 256, NULL, 0, NULL );

		/* 启动调度程序，以便开始执行创建的任务。 */
		vTaskStartScheduler();
	}

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建一个队列。

创建两个打印任务用于发送消息给看守任务。

创建一个看守任务，用于接收消息并打印输出。

然后启动OS。



```
static void prvPrintTask( void *pvParameters )
{
int iIndexToString;

	/* 该任务的两个实例被创建，因此任务将发送给看守任务的字符串的索引在任务参数中传递。 
	将此转换为所需的类型。 */
	iIndexToString = ( int ) pvParameters;

	for( ;; )
	{
		/* 打印出字符串，不是直接打印，而是通过将字符串传递给队列中的看守任务。 
		该队列是在调度程序启动之前创建的，因此在该任务执行时已经存在。 
		没有指定阻塞时间，因为队列中应该总是有空间。 */
		xQueueSendToBack( xPrintQueue, &( pcStringsToPrint[ iIndexToString ] ), 0 );

		/* 等待一个伪随机时间。 
		请注意， rand() 不一定是可重入的，但在这种情况下，它并不重要，因为代码并不关心返回什么值。 
		在更安全的应用程序中，应使用已知可重入的rand()版本，或者应使用临界区保护对rand()的调用。 */
		vTaskDelay( ( rand() % xMaxBlockTimeTicks ) );
	}
}
```

打印任务功能是，发送消息到队列xPrintQueue，并根据rand()函数返回值，进行随机延时。



```
static void prvStdioGatekeeperTask( void *pvParameters )
{
char *pcMessageToPrint;

	/* 这是唯一允许写入终端输出的任务。 
	任何其他想要写入输出的任务都不会直接访问终端，而是将输出发送到该任务。 
	由于只有一个任务写入标准输出，因此在此任务内无需考虑互斥或序列化问题。 */
	for( ;; )
	{
		/* 等待消息到达。 */
		xQueueReceive( xPrintQueue, &pcMessageToPrint, portMAX_DELAY );

		/* 无需检查返回值，因为任务将无限期阻塞，并且仅在消息到达时再次运行。 
		执行下一行时，将输出一条消息。 */
		printf( "%s", pcMessageToPrint );
		fflush( stdout );

		/* 现在只需返回等待下一条消息。 */
	}
}
```

看守任务功能是，从队列xPrintQueue中读取消息，并打印输出。



```
void vApplicationTickHook( void )
{
static int iCount = 0;

	/* 每 200 个滴答打印出一条消息。 
	消息不是直接写出来的，而是发送给看守任务的。 */
	iCount++;
	if( iCount >= 200 )
	{
		/* 在这种情况下，最后一个参数 (xHigherPriorityTaskWoken) 并未实际使用，而是设置为 NULL。 */
		xQueueSendToFrontFromISR( xPrintQueue, &( pcStringsToPrint[ 2 ] ), NULL );

		/* 重置计数准备在 200 滴答时间内再次打印出字符串。 */
		iCount = 0;
	}
}
```

当宏configUSE_TICK_HOOK配置为1时，每个Tick心跳就会调用一次TICK钩子函数。

在本例中它的功能是，每200个心跳，发送一次消息到队列xPrintQueue。



**知识点**：

1.在FreeRTOS中，Tick钩子函数的原型为void vApplicationTickHook( void )。

当宏configUSE_TICK_HOOK配置为1时，每个Tick心跳就会调用一次该函数。

2.TICK钩子函数，是在中断中调用的，所以要使用带FromISR的API。



**Malloc失败钩子函数:(扩展知识)**

```
/* 在其他示例中，此函数在 support_functions.c 源文件中实现 - 
但该源文件未包含在此示例中，因为包含它会导致刻度挂钩函数的多个定义。 */
void vApplicationMallocFailedHook( void )
{
	/* 仅当 FreeRTOSConfig.h 中的 configUSE_MALLOC_FAILED_HOOK 
	设置为 1 时才会调用 vApplicationMallocFailedHook()。 
	这是一个钩子函数，如果对 pvPortMalloc() 的调用失败，它将被调用。 
	每当创建任务、队列、计时器、事件组或信号量时，内核都会在内部调用 pvPortMalloc()。 
	它也被演示应用程序的各个部分调用。 如果使用 heap_1.c、heap_2.c 或 heap_4.c，
	则 pvPortMalloc() 可用的堆大小由 FreeRTOSConfig.h 中的 configTOTAL_HEAP_SIZE 定义，
	可使用 xPortGetFreeHeapSize() API 函数查询 剩余的可用堆空间。 
	本书正文中提供了更多信息。 */
	vAssertCalled( __LINE__, __FILE__ );
}
```

Malloc失败钩子函数的原型为void vApplicationMallocFailedHook( void )。

把宏configUSE_MALLOC_FAILED_HOOK配置为1，申请内存失败时就会调用该函数。

本例中，Malloc失败钩子函数调用了用户定义的vAssertCalled()：

```
/* 在其他示例中，此函数在 support_functions.c 源文件中实现 - 
但该源文件未包含在此示例中，因为包含它会导致刻度挂钩函数的多个定义。 */
void vAssertCalled( uint32_t ulLine, const char * const pcFile )
{
/* 以下两个变量只是为了确保参数没有被优化掉，因此在调试器中查看时不可用。 */
volatile uint32_t ulLineNumber = ulLine, ulSetNonZeroInDebuggerToReturn = 0;
volatile const char * const pcFileName = pcFile;

	taskENTER_CRITICAL();
	{
		while( ulSetNonZeroInDebuggerToReturn == 0 )
		{
			/* 如果您想在调试器中设置此函数以查看 assert() 位置，
			则将 ulSetNonZeroInDebuggerToReturn 设置为非零值。 */
		}
	}
	taskEXIT_CRITICAL();

	/* 消除由于变量已设置但随后未引用而发出编译器警告的可能性。 */
	( void ) pcFileName;
	( void ) ulLineNumber;
}
```

当申请内存失败后，将停留在该函数，用户可以通过pcFileName、ulLineNumber知道出错的文件名和行号。

本例中没有演示出错的情况。读者可以通过反复创建多个任务、队列等对象，来触发申请内存失败的事件。



The End.

2023-04-12.