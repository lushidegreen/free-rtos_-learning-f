## FreeRTOS学习笔记[24]Example022-事件组实验(Lu)

本篇主要是学习使用事件组实现任务间的同步，任务和中断的同步。

**概述**：

本例程演示事件组的使用。

一个任务设置事件Bit0和Bit1，并打印相应信息。

在中断函数中设置事件Bit2，并通过守护任务发送相应信息。

一个任务以“或”的方式等待事件，每接收到一个事件，打印相应的信息。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example022

把其中的main.c文件改为Example022.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example022分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example022/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example022

主要修改：

1.Example022.c中的main()函数名称改为 Example022()，然后在工程main.c文件中声明，并在初始化完成后调用 Example022()；

2.任务栈大小从1000改为256，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

3.在FreeRTOSConfig.h中配置使能软件定时器：

```
/* Software timer related configuration options. */
#define configUSE_TIMERS						1
#define configTIMER_TASK_PRIORITY				( configMAX_PRIORITIES - 1 ) /* Maximum possible priority. */
#define configTIMER_QUEUE_LENGTH				2
#define configTIMER_TASK_STACK_DEPTH			( configMINIMAL_STACK_SIZE * 2 )
```

并配置使能xTimerPendFunctionCall()函数，默认是禁止的：

```
#define INCLUDE_xTimerPendFunctionCall			1
```



#### Step3.编译下载运行。

把Example022.c添加到工程，然后编译。

下载运行，会持续输出字符串：

![1681388866466](image\1681388667353.png)

通过任务设置的是bit 0和bit 1，中断ISR设置的是bit 2。

注：本例中，是通过串口调试助手发送字符给MCU，来完成中断触发的。



#### Example022代码分析

```
/* 事件组中事件位的定义。 */
#define mainFIRST_TASK_BIT	( 1UL << 0UL ) /* 事件位 0，由任务设置。 */
#define mainSECOND_TASK_BIT	( 1UL << 1UL ) /* 事件位 1，由任务设置。 */
#define mainISR_BIT			( 1UL << 2UL ) /* 事件位 2，由任务设置。 */

/* 要创建的任务。 */
static void vEventBitSettingTask( void *pvParameters );
static void vEventBitReadingTask( void *pvParameters );

/* 可以延迟在 RTOS 守护程序任务中运行的函数。 该函数使用 pvParameter1 参数打印出传递给它的字符串。 */
void vPrintStringFromDaemonTask( void *pvParameter1, uint32_t ulParameter2 );

/*-----------------------------------------------------------*/

/* 声明从任务和 ISR 都设置位的事件组。 */
EventGroupHandle_t xEventGroup;

extern UART_HandleTypeDef huart1; // 声明UART1句柄
static uint8_t uart_rxDat[10] = { 0 }; // 串口接收缓冲区

int Example022( void )
{
    /* 使能串口接收中断 */
    HAL_UART_Receive_IT(&huart1, uart_rxDat, 1); 

	/* 在可以使用事件组之前，必须先创建它。 */
	xEventGroup = xEventGroupCreate();

	/* 创建在事件组中设置事件位的任务。 */
	xTaskCreate( vEventBitSettingTask, "BitSetter", 256, NULL, 1, NULL );

	/* 创建等待事件位在事件组中设置的任务。 */
	xTaskCreate( vEventBitReadingTask, "BitReader", 256, NULL, 2, NULL );

	/* 启动调度程序，以便开始执行创建的任务。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

使能串口接收中断。

创建一个事件组。

创建一个任务用于设置事件组。

创建一个任务用于等待事件组。

然后启动OS。



```
static void vEventBitSettingTask( void *pvParameters )
{
const TickType_t xDelay200ms = pdMS_TO_TICKS( 200UL );

	for( ;; )
	{
		/* 在开始下一个循环之前延迟一小会。 */
		vTaskDelay( xDelay200ms );

		/* 打印一条消息说事件位 0 即将由任务设置，然后设置事件位 0。 */
		vPrintString( "Bit setting task -\t about to set bit 0.\r\n" );
		xEventGroupSetBits( xEventGroup, mainFIRST_TASK_BIT );

		/* 在设置此任务中设置的其他位之前延迟片刻。 */
		vTaskDelay( xDelay200ms );

		/* 打印一条消息说事件位 1 即将被任务设置，然后设置事件位 1。 */
		vPrintString( "Bit setting task -\t about to set bit 1.\r\n" );
		xEventGroupSetBits( xEventGroup, mainSECOND_TASK_BIT );
	}
}
```

vEventBitSettingTask()任务功能是，设置事件Bit0和Bit1，并打印相应信息。



```
static void vEventBitReadingTask( void *pvParameters )
{
const EventBits_t xBitsToWaitFor = ( mainFIRST_TASK_BIT | mainSECOND_TASK_BIT | mainISR_BIT );
EventBits_t xEventGroupValue;

	for( ;; )
	{
		/* 阻止以等待事件位在事件组中设置。 */
		xEventGroupValue = xEventGroupWaitBits( /* 要读取的事件组。 */
												xEventGroup,

												/* 要测试的位。 */
												xBitsToWaitFor,

												/* 如果满足解除阻塞条件，则在退出时清除位。*/
												pdTRUE,

												/* 不要等待所有位。 */
												pdFALSE,

												/* 不要超时。 */
												portMAX_DELAY );

		/* 为每个设置的位打印一条消息。 */
		if( ( xEventGroupValue & mainFIRST_TASK_BIT ) != 0 )
		{
			vPrintString( "Bit reading task -\t event bit 0 was set\r\n" );
		}

		if( ( xEventGroupValue & mainSECOND_TASK_BIT ) != 0 )
		{
			vPrintString( "Bit reading task -\t event bit 1 was set\r\n" );
		}

		if( ( xEventGroupValue & mainISR_BIT ) != 0 )
		{
			vPrintString( "Bit reading task -\t event bit 2 was set\r\n" );
		}

		vPrintString( "\r\n" );
	}
}
```

vEventBitReadingTask()任务以“或”的方式等待事件，每接收到一个事件，打印相应的信息。



```
/* 中断的服务程序。 这是在事件组中设置事件位的中断。 */
static void ulEventBitSettingISR( void )
{
BaseType_t xHigherPriorityTaskWoken;
/* 该字符串不在中断服务中打印，而是发送到 RTOS 守护程序任务进行打印。 
因此，它被声明为静态以确保编译器不会在 ISR 的堆栈上分配字符串
（因为当从守护程序任务打印字符串时，ISR 的堆栈帧将不存在。） */
static const char *pcString = "Bit setting ISR -\t about to set bit 2.\r\n";

	/* 与往常一样，xHigherPriorityTaskWoken 被初始化为 pdFALSE。 */
	xHigherPriorityTaskWoken = pdFALSE;

	/* 打印出一条消息说第 2 位即将被设置。 
	无法从 ISR 打印消息，因此通过挂起函数调用以在 RTOS 守护程序任务的上下文中运行，
	将实际输出推迟到 RTOS 守护程序任务。 */
	xTimerPendFunctionCallFromISR( vPrintStringFromDaemonTask, ( void * ) pcString, 0, &xHigherPriorityTaskWoken );

	/* 设置事件组中的位 2。 */
	xEventGroupSetBitsFromISR( xEventGroup, mainISR_BIT, &xHigherPriorityTaskWoken );

	/* xEventGroupSetBitsFromISR() 写入定时器命令队列。 
	如果写入定时器命令队列导致 RTOS 守护任务离开阻塞状态，
	并且如果 RTOS 守护任务的优先级高于当前执行任务（被中断的任务）的优先级，
	则 xHigherPriorityTaskWoken 将被设置 到 xEventGroupSetBitsFromISR() 内的 pdTRUE。

	xHigherPriorityTaskWoken 用作 portYIELD_FROM_ISR() 的参数。 
	如果 xHigherPriorityTaskWoken 等于 pdTRUE，则调用 portYIELD_FROM_ISR() 将请求上下文切换。 
	如果 xHigherPriorityTaskWoken 仍为 pdFALSE，则调用 portYIELD_FROM_ISR() 将无效。

	Windows 端口使用的 portYIELD_FROM_ISR() 的实现包括一个返回语句，这就是该函数没有显式返回值的原因。 */
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
```

这是中断处理函数：(本例中，在串口接收中断中被调用。)

1.无法从ISR打印消息，所以通过xTimerPendFunctionCallFromISR()推迟到RTOS守护任务打印。

2.设置事件组的bit 2。



```
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if (USART1 == huart->Instance) {
        ulEventBitSettingISR();
        HAL_UART_Receive_IT(&huart1, uart_rxDat, 1); // 重新使能串口接收中断
    }
}
```

void HAL_UART_RxCpltCallback()，是STM32的HAL库指定的串口接收完成回调函数。

在使用HAL库编写的UART程序中，调用HAL_UART_Receive_IT()使能中断之后，接收完成才会调用回调函数。

而且，在接收完成中断中，必须调用HAL_UART_Receive_IT()重新使能中断，下次接收才会调用回调函数。



```
void vPrintStringFromDaemonTask( void *pvParameter1, uint32_t ulParameter2 )
{
	/* 使用 pvParameter1 参数将要打印的字符串传递给此函数。 */
	vPrintString( ( const char * ) pvParameter1 );
}
```

这是延迟处理函数，功能很简单，就是打印出从中断传递过来的字符串。



**知识点**：

1.创建事件组，使用xEventGroupCreate()。

2.设置事件组指定的bit，使用xEventGroupSetBits()，可以同时设置一个或多个bit。

3.在中断中设置事件组，使用xEventGroupSetBitsFromISR()。

4.等待事件组，使用xEventGroupWaitBits()，以“或”的方式，或者“与”的方式等待事件，由倒数第二个参数决定。



The End.

2023-04-13.