## FreeRTOS学习笔记[25]Example023-同步任务(Lu)

本篇主要是学习使用事件组实现多个任务的同步。

**概述**：

本例程演示使用事件组实现多个任务的同步。

创建3个任务，他们的任务函数相同。

3个任务分别调用xEventGroupSync()设置事件组的Bit0、Bit1和Bit2，并以“与”的方式等待时间组的Bit0、Bit1和Bit2，进入阻塞状态。

任务函数中使用伪随机数进行延时，用于模拟3个任务的不同步过程。

但最终，他们几乎在相同的时刻退出阻塞状态，从而达到任务同步的效果。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example023

把其中的main.c文件改为Example023.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example023分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example023/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example023

主要修改：

1.Example023.c中的main()函数名称改为 Example023()，然后在工程main.c文件中声明，并在初始化完成后调用 Example023()；

2.任务栈大小从1000改为256，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

3.删除prvRand()和prvSRand()，用stdlib库中的rand()来实现随机数生成。



#### Step3.编译下载运行。

把Example023.c添加到工程，然后编译。

下载运行，会输出字符串：

![1681396351863](image\1681396351863.png)

Task 1，Task 2，Task 3依次到达同步节点："reached sync point"。

然后3个任务基本同时退出同步点："exited sync point"。



#### Example023代码分析

```
/* 事件组中事件位的定义。 */
#define mainFIRST_TASK_BIT	( 1UL << 0UL ) /* 事件位 0，由第一个任务设置。 */
#define mainSECOND_TASK_BIT	( 1UL << 1UL ) /* 事件位 1，由第二个任务设置。 */
#define mainTHIRD_TASK_BIT	( 1UL << 2UL ) /* 事件位 2，由第三个任务设置。 */

/* 将创建此任务的三个实例。 */
static void vSyncingTask( void *pvParameters );

/*-----------------------------------------------------------*/

/* 声明用于同步三个任务的事件组。 */
EventGroupHandle_t xEventGroup;

int Example023( void )
{
	/* 在可以使用事件组之前，必须先创建它。 */
	xEventGroup = xEventGroupCreate();

	/* 创建任务的三个实例。 
	每个任务都有一个不同的名称，稍后会打印出来以直观地指示正在执行的任务。 
	当任务到达其同步点时使用的事件位使用任务参数传递给任务。 */
	xTaskCreate( vSyncingTask, "Task 1", 256, ( void * ) mainFIRST_TASK_BIT, 1, NULL );
	xTaskCreate( vSyncingTask, "Task 2", 256, ( void * ) mainSECOND_TASK_BIT, 1, NULL );
	xTaskCreate( vSyncingTask, "Task 3", 256, ( void * ) mainTHIRD_TASK_BIT, 1, NULL );

	/* 启动调度程序，以便开始执行创建的任务。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建一个事件组。

创建3个任务用于设置并等待事件组，它们使用相同的任务函数。

然后启动OS。



```
static void vSyncingTask( void *pvParameters )
{
const EventBits_t uxAllSyncBits = ( mainFIRST_TASK_BIT | mainSECOND_TASK_BIT | mainTHIRD_TASK_BIT );
const TickType_t xMaxDelay = pdMS_TO_TICKS( 4000UL );
const TickType_t xMinDelay = pdMS_TO_TICKS( 200UL );
TickType_t xDelayTime;
EventBits_t uxThisTasksSyncBit;

	/* 创建此任务的三个实例 - 每个任务在同步中使用不同的事件位。 
	此任务实例使用的事件位使用任务的参数传递给任务。 
	将其存储在 uxThisTasksSyncBit 变量中。 */
	uxThisTasksSyncBit = ( EventBits_t ) pvParameters;

	for( ;; )
	{
		/* 通过延迟伪随机时间来模拟此任务需要一些时间来执行操作。 
		这可以防止此任务的所有三个实例同时到达同步点，并允许更容易地观察示例的行为。 */
		xDelayTime = ( rand() % xMaxDelay ) + xMinDelay;
		vTaskDelay( xDelayTime );

		/* 打印一条消息以显示此任务已达到其同步点。 
		pcTaskGetTaskName() 是一个 API 函数，它返回创建任务时分配给任务的名称。 */
		vPrintTwoStrings( pcTaskGetTaskName( NULL ), "reached sync point" );

		/* 等待所有任务都达到各自的同步点。 */
		xEventGroupSync( /* 用于同步的事件组。 */
						 xEventGroup,

						 /* 此任务设置的位表示它已到达同步点。 */
						 uxThisTasksSyncBit,

						 /* 要等待的位，每个参与同步的任务一个位。 */
						 uxAllSyncBits,

						 /* 无限期地等待所有三个任务到达同步点。 */
						 portMAX_DELAY );

		/* 打印一条消息以显示此任务已通过其同步点。 
		由于使用了无限延迟，因此只有在所有任务都达到各自的同步点后才会到达以下行。 */
		vPrintTwoStrings( pcTaskGetTaskName( NULL ), "exited sync point" );
	}
}
```

3个任务分别调用xEventGroupSync()设置事件组的Bit0、Bit1和Bit2，并以“与”的方式等待时间组的Bit0、Bit1和Bit2，进入阻塞状态。

任务函数中使用伪随机数进行延时，用于模拟3个任务的不同步过程。



**知识点**：

通过xEventGroupSync()可以实现多个任务的同步。



The End.

2023-04-13.