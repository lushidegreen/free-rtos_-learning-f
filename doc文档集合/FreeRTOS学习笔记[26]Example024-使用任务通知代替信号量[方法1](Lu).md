## FreeRTOS学习笔记[26]Example024-使用任务通知代替信号量[方法1] (Lu)

本篇主要是学习使用任务通知(TaskNotify)，实现任务和中断的同步。

Example016中，使用信号量来实现任务和中断的同步，而本例程把信号量替换成任务通知。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example024

把其中的main.c文件改为Example024.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example024分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example024/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example024

主要修改：

1.Example024.c中的main()函数名称改为 Example024()，然后在工程main.c文件中声明，并在初始化完成后调用 Example024()；

2.Example024.c中删除用于Windows环境下模拟中断的vPeriodicTask()任务和vPortSetInterruptHandler()配置。

本例程在STM32平台下，使用硬件TIM2定时中断来替代。

3.在Example024()中，调用vTaskStartScheduler()启动调度器前，启动TIM1并使能中断：

```
	/* 设置TIM2计数器,这里设置为100,是为了避免启动调度器前产生TIM中断 */
	__HAL_TIM_SET_COUNTER(&htim2, 100);
	/* 启动TIM2并使能中断 */
	HAL_TIM_Base_Start_IT(&htim2);
```

4.在工程main.c文件中声明ulExampleInterruptHandler()，并在TIM中断回调函数中调用该函数。

```
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  // ......
  if (htim->Instance == TIM2) {
    ulExampleInterruptHandler();
  }
  // ......
}
```



#### Step3.编译下载运行。

把Example024.c添加到工程，然后编译。

下载运行，每隔1秒钟输出字符串“Handler task - Processing event.”。

![1681398239335](image\1681398239335.png)



#### Example024代码分析

```
/* 要创建的任务。 */
static void vHandlerTask( void *pvParameters );

extern TIM_HandleTypeDef htim2;

/* 定时器TIM2产生中断的速率。 */
static const TickType_t xInterruptFrequency = pdMS_TO_TICKS( 1000UL );

/* 存储延迟处理中断的任务的句柄。 */
static TaskHandle_t xHandlerTask = NULL;

/*-----------------------------------------------------------*/

int Example024( void )
{
	/* 创建“处理程序”任务，这是中断处理被推迟到的任务，将与中断同步的任务也是如此。 
	处理程序任务以高优先级创建，以确保它在中断退出后立即运行。 
	在这种情况下，选择优先级 3。 任务的句柄被保存以供 ISR 使用。 */
	xTaskCreate( vHandlerTask, "Handler", 1000, NULL, 3, &xHandlerTask );

	/* 设置TIM2计数器,这里设置为100,是为了避免启动调度器前产生TIM中断 */
	__HAL_TIM_SET_COUNTER(&htim2, 100);
	/* 启动TIM2并使能中断 */
	HAL_TIM_Base_Start_IT(&htim2);

	/* 启动调度程序，以便开始执行创建的任务。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建1个任务用于接收任务通知(TaskNotify)并打印信息。

启动TIM1并使能中断，然后启动OS。



```
static void vHandlerTask( void *pvParameters )
{
/* xMaxExpectedBlockTime 设置为比事件之间的最大预期时间稍长。 */
const TickType_t xMaxExpectedBlockTime = xInterruptFrequency + pdMS_TO_TICKS( 10 );
uint32_t ulEventsToProcess;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 等待接收从中断处理程序直接发送到此任务的通知。 */
		ulEventsToProcess = ulTaskNotifyTake( pdTRUE, xMaxExpectedBlockTime );
		if( ulEventsToProcess != 0 )
		{
			/* 要到达这里，至少必须发生一件事。 
			在此循环，直到处理完所有未决事件（在这种情况下，只需为每个事件打印一条消息）。 */
			while( ulEventsToProcess > 0 )
			{
				vPrintString( "Handler task - Processing event.\r\n" );
				ulEventsToProcess--;
			}
		}
		else
		{
			/* 如果达到了这部分功能，则中断没有在预期时间内到达，
			并且（在实际应用中）可能需要执行一些错误恢复操作。 */
		}
	}
}
```

功能很简单，就是等待任务通知，然后打印一条信息。



```
void ulExampleInterruptHandler( void )
{
BaseType_t xHigherPriorityTaskWoken;

	/* xHigherPriorityTaskWoken 参数必须初始化为 pdFALSE，
	因为如果需要上下文切换，它将在中断安全 API 函数内设置为 pdTRUE。 */
	xHigherPriorityTaskWoken = pdFALSE;

	/* 直接向处理程序任务发送通知。 */
	vTaskNotifyGiveFromISR( /* 向其发送通知的任务的句柄。 创建任务时已保存句柄。 */
							xHandlerTask,

							/* xHigherPriorityTaskWoken 以通常的方式使用。 */
							&xHigherPriorityTaskWoken );

	/* 将 xHigherPriorityTaskWoken 值传递给 portYIELD_FROM_ISR()。 
	如果 xHigherPriorityTaskWoken 在 vTaskNotifyGiveFromISR() 中设置为 pdTRUE，
	则调用 portYIELD_FROM_ISR() 将请求上下文切换。 
	如果 xHigherPriorityTaskWoken 仍为 pdFALSE，则调用 portYIELD_FROM_ISR() 将无效。 
	Windows 端口使用的 portYIELD_FROM_ISR() 的实现包括一个返回语句，这就是该函数没有显式返回值的原因。 */
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
```

发送任务通知给任务xHandlerTask。



```
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
  if (htim->Instance == TIM2) {
    ulExampleInterruptHandler();
  }
  /* USER CODE END Callback 1 */
}
```

void HAL_TIM_PeriodElapsedCallback()，是STM32的HAL库指定的所有定时器周期中断回调函数。在该函数中，通过htim->Instance判断当前中断所对应的定时器。

在前面构建基础工程时，把HAL时基改成了TIM1，所以这里调用了HAL_IncTick()，以实现HAL的各种超时机制。

在使用HAL库编写的TIM程序中，调用HAL_TIM_Base_Start_IT()使能中断之后，定时时间到才会调用回调函数。



**知识点**：

1.任务通知比信号量节省资源，它是和任务绑定的，不需要显性的创建。

2.等待任务通知使用ulTaskNotifyTake()。

3.在中断中发送任务通知使用vTaskNotifyGiveFromISR()，在任务中发送任务通知使用vTaskNotifyGive()。

4.在中断中调用vTaskNotifyGiveFromISR()后，把第二个参数输出值传给portYIELD_FROM_ISR()。这是因为发送任务通知后，可能会激活等待的任务，在退出后就需要切换上下文。



The End.

2023-04-13.