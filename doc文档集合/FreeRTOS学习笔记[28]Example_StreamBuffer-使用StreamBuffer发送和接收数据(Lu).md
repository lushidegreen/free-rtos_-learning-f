## FreeRTOS学习笔记[28]Example_StreamBuffer-使用StreamBuffer发送和接收数据(Lu)

本篇主要是学习使用StreamBuffer发送和接收数据。

#### Step1.从官方例程中，复制源文件到工程

从Example010分支，复制Example010.c文件到工程，更名为Example_StreamBuffer.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example_StreamBuffer分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example_StreamBuffer/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example_StreamBuffer

主要修改：

1.Example_StreamBuffer.c中的Example010()函数名称改为 Example_StreamBuffer()，然后在工程main.c文件中声明，并在初始化完成后调用 Example_StreamBuffer()；

2.包含两个头文件：

```
#include "stream_buffer.h"
#include "string.h"
```

3.参考《FreeRTOS_Reference_Manual_V10.0.0.pdf》“Chapter 8  Stream Buffer API”中以下3个API的相关例程：xStreamBufferCreate()、xStreamBufferSend()、xStreamBufferReceive()。修改Example_StreamBuffer()、vSenderTask()、vReceiverTask()的实现代码，详细代码见后面的“代码分析”部分。

#### Step3.编译下载运行。

把Example_StreamBuffer.c添加到工程，把../FreeRTOS/stream_buffer.c也添加到工程，然后编译。

下载运行，会持续输出字符串，每秒钟输出2行。

![1681909077408](image\1681909077408.png)

#### Example_StreamBuffer代码分析

```
/* 要创建的任务。 为发送者任务创建了一个实例，而只为接收者任务创建了一个实例。 */
static void vSenderTask( void *pvParameters );
static void vReceiverTask( void *pvParameters );

/*-----------------------------------------------------------*/

/* 声明一个 QueueHandle_t 类型的变量。 这用于存储所有三个任务都访问的队列。 */
StreamBufferHandle_t xStreamBuffer;


int Example_StreamBuffer( void )
{
	const size_t xStreamBufferSizeBytes = 100, xTriggerLevel = 10;

    /* 创建一个可以容纳100字节的流缓冲区。用于保存流缓冲区结构和流缓冲区中的数据的内存是动态分配的。*/
    xStreamBuffer = xStreamBufferCreate( xStreamBufferSizeBytes, xTriggerLevel );

	if( xStreamBuffer != NULL )
	{
		/* 创建将写入队列的任务的一个实例。 这个任务都以优先级 1 创建。 */
		xTaskCreate( vSenderTask, "Sender1", 256, NULL, 1, NULL );

		/* 创建将从队列中读取的任务。 该任务以优先级 2 创建，因此高于发送者任务的优先级。 */
		xTaskCreate( vReceiverTask, "Receiver", 256, NULL, 2, NULL );

		/* 启动调度程序，以便开始执行创建的任务。 */
		vTaskStartScheduler();
	}
	else
	{
		/* 无法创建队列。 */
	}

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建一个可以容纳100字节的流缓冲区。

创建1个发送任务，优先级都是1。

创建1个接收任务，优先级为2，比发送任务高。

然后启动OS执行任务。



```
static void vSenderTask( void *pvParameters )
{ 
/* 为了方便演示, 发送的数据都使用可打印字符 */
uint8_t ucArrayToSend[] = { 'A', 'B', 'C', 'D' };
char *pcStringToSend = "String to send";
size_t xBytesSent;
const TickType_t x100ms = pdMS_TO_TICKS( 100 );

	( void ) pvParameters;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 发送一个数组到流缓冲区，阻塞最多100ms，以等待流缓冲区中有足够的可用空间。 */
		xBytesSent = xStreamBufferSend( xStreamBuffer,
										( void * ) ucArrayToSend,
										sizeof( ucArrayToSend ),
										x100ms );
		if( xBytesSent != sizeof( ucArrayToSend ) )
		{
			/* 在缓冲区中有足够的空间用于写入数据之前，
			对xStreamBufferSend()的调用超时了，
			但它确实成功地写入了xBytesSent字节。 */
			vPrintString( "Could not send Array to the StreamBuffer.\r\n" );
		}
		vTaskDelay(500);
		/* 将字符串发送到流缓冲区。
		如果缓冲区中没有足够的空间，立即返回。 */
		xBytesSent = xStreamBufferSend( xStreamBuffer,
										( void * ) pcStringToSend,
										strlen( pcStringToSend ), 
										0 );
		if( xBytesSent != strlen( pcStringToSend ) )
		{
			/* 无法将整个字符串添加到流缓冲区，因为缓冲区中没有足够的可用空间，
			但是发送了xBytesSent字节。可以再次尝试发送剩余的字节。 */
			vPrintString( "Could not send String to the StreamBuffer.\r\n" );
		}
		vTaskDelay(500);
	}
}
```

任务功能很简单，就是把数据发送到流缓冲区。

发送的数据有2组， { 'A', 'B', 'C', 'D' }和"String to send"，发送时间间隔为500ms。



```
static void vReceiverTask( void *pvParameters )
{
/* 声明将保存从流缓冲区接收到的数据的数组。 */
#define	RX_BUFFER_SIZE	20
uint8_t ucRxData[ RX_BUFFER_SIZE + 1];
size_t xReceivedBytes;
const TickType_t xBlockTime = pdMS_TO_TICKS( 100 );

	/* 此任务也在无限循环中定义。 */
	for( ;; )
	{
		/* 从流缓冲区接收另一个RX_BUFFER_SIZE字节。
		在Blocked状态下(因此不使用任何CPU处理时间)等待最多100ms，
		以获得完整的(ucRxData)字节数。 */
		xReceivedBytes = xStreamBufferReceive( xStreamBuffer,
												( void * ) ucRxData,
												RX_BUFFER_SIZE,
												xBlockTime );
		if( xReceivedBytes > 0 )
		{
			ucRxData[xReceivedBytes] = '\0'; // 字符串结束符
			vPrintString( "Received: " );
			vPrintString( (const char *)ucRxData ); // 打印接收到的字符串数据
			vPrintString( "\r\n" );
		}
	}
}
```

任务从流缓冲区读取数据并打印输出。



**知识点**：

1.创建流缓冲区使用xStreamBufferCreate()。

2.向流缓冲区发送数据，使用xStreamBufferSend()。

3.从流缓冲区接收数据，使用xStreamBufferReceive()。



4.xStreamBufferCreate()第一个参数指定流缓冲区大小(字节)；第二个参数指定流缓冲区的触发级别(字节)。

触发级别设为0时，被自动设为1；触发级别设置大于流缓冲区大小，将无效。

 		例如，一个任务在读取一个触发级别为1的空流缓冲区时被阻塞，那么当一个字节被写入缓冲区或任务的阻塞时间到期时，该任务将被解除阻塞。  

​		再如，一个任务在读取一个触发级别为10的空流缓冲区时被阻塞，那么在流缓冲区包含至少10个字节或任务的块时间到期之前，该任务将不会被解除阻塞。 



5.StreamBuffer是针对一个writer和一个reader的应用场景，如果是多个writer或reader，则需要读写函数xStreamBufferSend()和xStreamBufferReceive()需要放在临界区中操作，才能确保安全。

6.在中断中发送和接收流缓冲区，分别使用xStreamBufferSendFromISR()、xStreamBufferReceiveFromISR()。

7.判断流缓冲区空和满，分别使用xStreamBufferIsEmpty()、xStreamBufferIsFull()。

8.获取当前流缓冲区中的数据量(字节)，使用xStreamBufferBytesAvailable()。

9.获取当前流缓冲区中的剩余空间(字节)，使用xStreamBufferSpacesAvailable()。

10.  xStreamBufferReset()可复位流缓冲区，当前未被读取的数据，将被清空。



The End.

2023-04-19.