## FreeRTOS学习笔记[29]Example_MessageBuffer-使用MessageBuffer发送和接收消息(Lu)

本篇主要是学习使用MessageBuffer发送和接收消息。

#### Step1.从官方例程中，复制源文件到工程

从Example_StreamBuffer分支，复制Example_StreamBuffer.c文件到工程，更名为Example_MessageBuffer.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example_MessageBuffer分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example_MessageBuffer/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example_MessageBuffer

主要修改：

1.Example_MessageBuffer.c中的Example_StreamBuffer()函数名称改为 Example_MessageBuffer()，然后在工程main.c文件中声明，并在初始化完成后调用 Example_MessageBuffer()；

2.包含两个头文件：

```
#include "message_buffer.h"
#include "string.h"
```

3.参考《FreeRTOS_Reference_Manual_V10.0.0.pdf》“Chapter 9  Message Buffer API”中以下3个API的相关例程：xMessageBufferCreate()、xMessageBufferSend()、xMessageBufferReceive()。修改Example_MessageBuffer()、vSenderTask()、vReceiverTask()的实现代码，详细代码见后面的“代码分析”部分。

#### Step3.编译下载运行。

把Example_MessageBuffer.c添加到工程，把../FreeRTOS/stream_buffer.c也添加到工程，然后编译。

下载运行，会持续输出字符串，每秒钟输出2行。

![1681914799984](image/1681914799984.png)

#### Example_MessageBuffer代码分析

```
/* 要创建的任务。 为发送者任务创建了一个实例，而只为接收者任务创建了一个实例。 */
static void vSenderTask( void *pvParameters );
static void vReceiverTask( void *pvParameters );

/*-----------------------------------------------------------*/

/* 声明一个 MessageBufferHandle_t 类型的变量。 这用于存储所有2个任务都访问的消息缓冲区。 */
MessageBufferHandle_t xMessageBuffer;


int Example_MessageBuffer( void )
{
	const size_t xMessageBufferSizeBytes = 100;

    /* 创建一个可以容纳100字节的消息缓冲区。
    用于保存消息缓冲区结构和消息缓冲区中的数据的内存是动态分配的。 */
	xMessageBuffer = xMessageBufferCreate( xMessageBufferSizeBytes );

	if( xMessageBuffer != NULL )
	{
		/* 创建将写入消息缓冲区的任务的一个实例。 这个任务都以优先级 1 创建。 */
		xTaskCreate( vSenderTask, "Sender1", 256, NULL, 1, NULL );

		/* 创建将从消息缓冲区中读取的任务。 该任务以优先级 2 创建，因此高于发送者任务的优先级。 */
		xTaskCreate( vReceiverTask, "Receiver", 256, NULL, 2, NULL );

		/* 启动调度程序，以便开始执行创建的任务。 */
		vTaskStartScheduler();
	}
	else
	{
		/* 无法创建消息缓冲区。 */
	}

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

创建一个可以容纳100字节的消息缓冲区。

创建1个发送任务，优先级都是1。

创建1个接收任务，优先级为2，比发送任务高。

然后启动OS执行任务。



```
static void vSenderTask( void *pvParameters )
{ 
/* 为了方便演示, 发送的数据都使用可打印字符 */
uint8_t ucArrayToSend[] = { 'A', 'B', 'C', 'D' };
char *pcStringToSend = "String to send";
size_t xBytesSent;
const TickType_t x100ms = pdMS_TO_TICKS( 100 );

	( void ) pvParameters;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 发送一个数组到消息缓冲区，阻塞最多100ms，以等待消息缓冲区中有足够的可用空间。 */
		xBytesSent = xMessageBufferSend( xMessageBuffer,
										( void * ) ucArrayToSend,
										sizeof( ucArrayToSend ),
										x100ms );
		if( xBytesSent != sizeof( ucArrayToSend ) )
		{
			/* 在缓冲区中有足够的空间用于写入数据之前，对xMessageBufferSend()的调用会超时。 */
			vPrintString( "Could not send Array to the MessageBuffer.\r\n" );
		}
		vTaskDelay(500);
		/* 将字符串发送到消息缓冲区。
		如果缓冲区中没有足够的空间，立即返回。 */
		xBytesSent = xMessageBufferSend( xMessageBuffer,
										( void * ) pcStringToSend,
										strlen( pcStringToSend ), 
										0 );
		if( xBytesSent != strlen( pcStringToSend ) )
		{
			/* 无法将字符串添加到消息缓冲区，因为缓冲区中没有足够的可用空间。 */
			vPrintString( "Could not send String to the MessageBuffer.\r\n" );
		}
		vTaskDelay(500);
	}
}
```

任务功能很简单，就是把数据发送到消息缓冲区。

发送的数据有2组， { 'A', 'B', 'C', 'D' }和"String to send"，发送时间间隔为500ms。



```
static void vReceiverTask( void *pvParameters )
{
/* 声明将保存从消息缓冲区接收到的数据的数组。 */
#define	RX_BUFFER_SIZE	20
uint8_t ucRxData[ RX_BUFFER_SIZE + 1];
size_t xReceivedBytes;
const TickType_t xBlockTime = pdMS_TO_TICKS( 100 );

	/* 此任务也在无限循环中定义。 */
	for( ;; )
	{
		/* 从消息缓冲区接收下一条消息。
		在Blocked状态下(因此不使用任何CPU处理时间)等待消息的时间最多为100ms。 */
		xReceivedBytes = xMessageBufferReceive( xMessageBuffer,
												( void * ) ucRxData,
												RX_BUFFER_SIZE,
												xBlockTime );
		if( xReceivedBytes > 0 )
		{
			ucRxData[xReceivedBytes] = '\0'; // 字符串结束符
			vPrintString( "Received: " );
			vPrintString( (const char *)ucRxData ); // 打印接收到的字符串数据
			vPrintString( "\r\n" );
		}
	}
}
```

任务从消息缓冲区读取数据并打印输出。



**知识点**：

1.创建消息缓冲区使用xMessageBufferCreate()。

```
    /* 创建一个可以容纳100字节的消息缓冲区。
    实际占用的内存空间是100字节+sizeof(size_t)=104字节 */
	xMessageBuffer = xMessageBufferCreate( xMessageBufferSizeBytes );
```

2.向消息缓冲区发送数据，使用xMessageBufferSend()。

```
uint8_t ucArrayToSend[] = { 'A', 'B', 'C', 'D' };
size_t xBytesSent;
const TickType_t x100ms = pdMS_TO_TICKS( 100 );

xBytesSent = xMessageBufferSend( xMessageBuffer,
								( void * ) ucArrayToSend,
								sizeof( ucArrayToSend ),
								x100ms );
```

虽然数组ucArrayToSend[]大小是4字节，但是把这个消息发送后，它消耗的消息缓冲区空间是：

4字节++sizeof(size_t)=8字节。



3.从消息缓冲区接收数据，使用xMessageBufferReceive()，是以“消息”为单位的。每次至少读取一条“消息”。

参数xBufferLengthBytes是pvRxData参数所指向的缓冲区的长度。xBufferLengthBytes设置了可以接收的消息的最大长度。如果该参数的值太小而不能容纳下一条消息，则消息将留在消息缓冲区中并返回0。

从消息缓冲区读取数据，是以“消息”为单位的。



4.MessageBuffer是针对一个writer和一个reader的应用场景，如果是多个writer或reader，则需要读写函数xMessageBufferSend()和xMessageBufferReceive()需要放在临界区中操作，才能确保安全。

5.在中断中发送和接收消息缓冲区，分别使用xMessageBufferSendFromISR()、xMessageBufferReceiveFromISR()。

6.判断消息缓冲区空和满，分别使用xMessageBufferIsEmpty()、xMessageBufferIsFull()。

7.获取当前消息缓冲区中的下一条消息长度(字节)，使用xMessageBufferNextLengthBytes()。

8.获取当前消息缓冲区中的剩余空间(字节)，使用xMessageBufferSpacesAvailable()。

9.xMessageBufferReset()可复位消息缓冲区，当前未被读取的数据，将被清空。



The End.

2023-04-19.