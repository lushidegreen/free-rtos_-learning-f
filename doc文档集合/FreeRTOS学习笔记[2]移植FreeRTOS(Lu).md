## FreeRTOS学习笔记[2]移植FreeRTOS(Lu)

本篇主要是移植FreeRTOS，把裸机工程变成带操作系统的工程。

#### Step1.下载FreeRTOS源码。

到官网首页https://www.freertos.org/，点击右上角图标进入下载页面。

![1680014679444](image/1680014679444.png)

有两个版本：最新版、LTS版(长期支持版)。

最新版包含了FreeRTOS内核、FreeRTOS+库、IoT库，以及示例工程。

LTS版只包含FreeRTOS内核和IoT库，而没有示例工程。

选择最新版下载。如果浏览器下载较慢，可以复制链接到迅雷下载：

https://github.com/FreeRTOS/FreeRTOS/releases/download/202212.01/FreeRTOSv202212.01.zip

压缩包有88MB，解压后超过500MB。所包含的内容，用到的时候再描述。

#### Step2.下载参考手册、官方教程及例程源代码。

https://www.freertos.org/Documentation/RTOS_book.html

![1680016174424](image/1680016174424.png)

源码压缩包很小，只有429kB，解压后也只有1.4MB。解压后如下：

![1680016973163](image/1680016973163.png)

从标题可以看出，这是一个基于windows平台模拟器的例程。后面学习的时候，会做一些小小的修改以使用与STM32平台。

 其中Examples文件夹中包含有25个示例：

![1680017099453](image/1680017099453.png)

**后面将主要参考这两个文档以及示例代码来学习。**

#### Step3.拷贝FreeRTOS源文件到工程。

在上一篇构建的MDK工程中创建 FreeRTOS 文件夹。

把.\FreeRTOSv202212.01\FreeRTOS\Source文件夹中的源码，全部复制到新建的FreeRTOS文件夹下。

![1680619791028](image/1680619791028.png)

#### Step4.添加FreeRTOS配置文件。

MDK工程中需要一个FreeRTOSConfig.h用于配置FreeRTOS。

这个文件并没有包含在内核源码中。

可以到最新版源码的Demo例程CORTEX_STM32F103_Keil中复制。

![1680620004786](image/1680620004786.png)

放到MDK工程用户头文件所在目录即可。

![1680620108968](image/1680620108968.png)

#### Step5.将FreeRTOS源码文件添加到MDK工程中。

![1680620379533](image/1680620379533.png)

其中heap_4.c文件路径：\FreeRTOS\portable\MemMang。

port.c文件的路径：\FreeRTOS\portable\RVDS\ARM_CM3。 

#### Step6.添加相关头文件路径到MDK工程。

![1680620631649](image/1680620631649.png)

#### Step7.修改 FreeRTOSConfig.h配置文件。

本篇只涉及少量的基本配置，后续再根据需要添加或修改相关配置。

1.CUP主频

```
#define configCPU_CLOCK_HZ			( ( unsigned long ) 72000000 )
```

2.添加宏定义__NVIC_PRIO_BITS，STM32 系列产品优先级仅使用4位；

```
/* Cortex-M specific definitions. */
#ifdef __NVIC_PRIO_BITS
 /* __BVIC_PRIO_BITS will be specified when CMSIS is being used. */
 #define configPRIO_BITS         __NVIC_PRIO_BITS
#else
 #define configPRIO_BITS         4
#endif
```

3.增加两个宏定义

```
#define configLIBRARY_LOWEST_INTERRUPT_PRIORITY   15
#define configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY   5
```

4.修改两个宏定义

```
#define configKERNEL_INTERRUPT_PRIORITY 		( configLIBRARY_LOWEST_INTERRUPT_PRIORITY << (8 - configPRIO_BITS) )

#define configMAX_SYSCALL_INTERRUPT_PRIORITY 	( configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY << (8 - configPRIO_BITS) )
```

5.把FreeRTOS的几个接口映射到STM32的中断向量上。

```
#define vPortSVCHandler SVC_Handler
#define xPortPendSVHandler PendSV_Handler
#define xPortSysTickHandler SysTick_Handler
```

其中 vPortSVCHandler，xPortPendSVHandler 和 xPortSysTickHandler 是在 port.c 文件里面定义的。
	SVC_Handler，PendSV_Handler 和 SysTick_Handler 在 startup_stm32f103x6.s 文件里面进行了定义。 

6.堆大小改为5kB (根据芯片SRAM大小调整，STM32F103C6T6只有10kB)

```
#define configTOTAL_HEAP_SIZE		( ( size_t ) ( 5 * 1024 ) )
```

#### Step8.屏蔽MDK工程中stm32f1xx_it.c/.h定义的下面3个函数。

SVC_Handler()、PendSV_Handler()、SysTick_Handler()。

这3个函数在STM32CubeMX构建的MDK工程代码中都有实现。

都是空函数，直接屏蔽即可。

**【至此，FreeRTOS 的移植工作就完成了，剩下就是系统配置和应用了。】**

#### Step9.把Printf()映射到UART口，并创建任务进行测试。

添加头文件

```
#include "stdio.h"
#include "FreeRTOS.h"
#include "task.h"
```

重定义fputc函数

```
int fputc(int ch, FILE *f)
{
	HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0xFFFF);
	return ch;
}
```

定义任务函数

```
void vTaskHello(void *pvParameters)
{
	while(1) {
		printf("Hello FreeRTOS!\r\n");
		HAL_Delay(1000);
	}
}
```

创建任务

```
  /* USER CODE BEGIN 2 */
  xTaskCreate( vTaskHello,/* 任务函数 */
			"vTaskHello", /* 任务名 */
			512, 		  /* 任务栈大小，单位word，即4字节 */
			NULL, 		  /* 任务参数 */
			2, 			  /* 任务优先级*/
			&xHandle); 	  /* 任务句柄 */

  vTaskStartScheduler();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
```

#### Step10.编译，下载运行。

![1680623888696](image/1680623888696.png)

#### Step11.添加例程的调试函数API

这一步，是为了方便后面的例程演示，减少对例程代码的修改。

```
void vPrintString( const char **pcString* )：输出一个字符串。

void vPrintStringAndNumber( const char **pcString*, uint32_t *ulValue* )：输出一个字符串及数字。

void vPrintTwoStrings( const char **pcString1*, const char **pcString2* )：先输出系统时间戳，然后输出两个字符串。
```

![1680624545016](image/1680624545016.png)

把教程源代码\Supporting_Functions文件夹复制到工程中，并把文件和头文件路径添加到工程。

![1680624849549](image/1680624849549.png)

![1680624796876](image/1680624796876.png)

把supporting_functions.c中调用_kbhit()的代码屏蔽，该函数用于windows模拟环境下键盘输入。STM32的实验中不使用。

把supporting_functions.c中调用vTaskEndScheduler()的代码屏蔽，该函数是停止调度器。后续的实验中也不使用。

#### Step12.测试调试函数API。

添加头文件

```
#include "supporting_functions.h"
```

任务函数改为

```
void vTaskHello(void *pvParameters)
{
    uint32_t ulValue = 0;

	while(1) {
		// printf("Hello FreeRTOS!\r\n");
		vPrintString("vPrintString().\r\n");
        vPrintStringAndNumber("ulValue = ", ulValue++);
        vPrintTwoStrings("String1","String2\r\n");
		HAL_Delay(1000);
	}
}
```

编译，下载运行，每秒钟串口输出3行信息：

![1680626633349](image/1680626633349.png)

【至此，移植和准备工作完成了。后面的笔记，都是在当前工程基础上，学习FreeRTOS的各种功能。】

完整工程代码连接：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/BaseVersion01

The End.

2023-04-05.