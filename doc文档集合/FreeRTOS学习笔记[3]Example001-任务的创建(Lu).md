## FreeRTOS学习笔记[3]Example001-任务的创建(Lu)

本篇主要是学习使用xTaskCreate()创建任务。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example001

把其中的main.c文件改为Example001.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example001分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example001/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example001

主要修改：

1.Example001.c中的main()函数名称改为 Example001()，然后在工程main.c文件中声明，并在初始化完成后调用 Example001()；

2.任务栈大小从1000改为512，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example001.c添加到工程，然后编译。

下载运行，大概每2秒输出两个字符串。

![1680698914342](image/1680698914342.png)

#### Example001代码分析

```
void vTask1( void *pvParameters )
{
const char *pcTaskName = "Task 1 is running\r\n";
volatile uint32_t ul;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 打印出此任务的名称。 */
		vPrintString( pcTaskName );

		/* 延迟一段时间。 */
		for( ul = 0; ul < mainDELAY_LOOP_COUNT; ul++ )
		{
			/* 这个循环只是一个非常粗糙的延迟实现。 这里没有做任何事情。 
			后面的练习将用适当的延迟/睡眠功能替换这个粗略的循环。 */
		}
	}
}
```

```
void vTask2( void *pvParameters )
{
const char *pcTaskName = "Task 2 is running\r\n";
volatile uint32_t ul;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 打印出此任务的名称。 */
		vPrintString( pcTaskName );

		/* 延迟一段时间。 */
		for( ul = 0; ul < mainDELAY_LOOP_COUNT; ul++ )
		{
			/* 这个循环只是一个非常粗糙的延迟实现。 这里没有做任何事情。 
			后面的练习将用适当的延迟/睡眠功能替换这个粗略的循环。 */
		}
	}
}
```

两个任务函数功能相似，都是无限循环中打印输出一个字符，延时使用for()循环实现。



```
int Example001( void )
{
	/* 创建两个任务之一。 */
	xTaskCreate(	vTask1,		/* 指向实现任务的函数的指针。 */
					"Task 1",	/* 任务的文本名称。 这只是为了方便调试。 */
					512,		/* 堆栈深度 - 大多数小型微控制器将使用比这少得多的堆栈。 */
					NULL,		/* 在此没有使用任务参数。 */
					1,			/* 此任务将以优先级 1 运行。 */
					NULL );		/* 在此没有使用任务句柄。 */

	/* 以完全相同的方式创建另一个任务。 */
	xTaskCreate( vTask2, "Task 2", 512, NULL, 1, NULL );

	/* 启动调度程序以开始执行任务。 */
	vTaskStartScheduler();	

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

例程入口函数功能也很简单，就是创建了两个任务后，启动调度器。



***知识点1**：xTaskCreate()创建任务时，如果不需要传递参数给任务，第四个参数可以输入NULL。

**知识点2**：xTaskCreate()创建任务时，如果外部程序不需要控制该任务(例如把任务挂起、恢复、删除等)，最后一个参数可以输入NULL。

**知识点3**：除了像上面一样，在启动调度器之前创建任务之外，还可以在任务中创建任务。



The End.

2023-04-05.