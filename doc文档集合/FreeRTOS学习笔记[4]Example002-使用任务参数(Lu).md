## FreeRTOS学习笔记[4]Example002-使用任务参数(Lu)

本篇主要是学习使用xTaskCreate()创建任务时传递参数。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example002

把其中的main.c文件改为Example002.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example002分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example002/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example002

主要修改：

1.Example002.c中的main()函数名称改为 Example002()，然后在工程main.c文件中声明，并在初始化完成后调用 Example002()；

2.任务栈大小从1000改为512，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example002.c添加到工程，然后编译。

下载运行，大概每2秒输出两个字符串。

![1680703602847](image/1680703602847.png)

#### Example002代码分析

```
void vTaskFunction( void *pvParameters )
{
char *pcTaskName;
volatile uint32_t ul;

	/* 要打印的字符串是通过参数传入的。 将此转换为字符指针。 */
	pcTaskName = ( char * ) pvParameters;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 打印出此任务的名称。 */
		vPrintString( pcTaskName );

		/* 延迟一段时间。 */
		for( ul = 0; ul < mainDELAY_LOOP_COUNT; ul++ )
		{
			/* 这个循环只是一个非常粗糙的延迟实现。 这里没有做任何事情。 
			后面的练习将用适当的延迟/睡眠功能替换这个粗略的循环。 */
		}
	}
}
```

两个任务使用相同的函数，都是无限循环中打印输出一个字符，延时使用for()循环实现。

通过pvParameters获取创建时传入的参数。因为传入的是字符串，所以需要强制转换成字符串再使用。



```
/* 定义将作为任务参数传入的字符串。 这些被定义为 const 并在堆栈外，以确保它们在任务执行时保持有效。 */
const char *pcTextForTask1 = "Task 1 is running\r\n";
const char *pcTextForTask2 = "Task 2 is running\r\n";

/*-----------------------------------------------------------*/
int Example002( void )
{
	/* 创建两个任务之一。 */
	xTaskCreate(	vTaskFunction,		    /* 指向实现任务的函数的指针。 */
					"Task 1",	            /* 任务的文本名称。 这只是为了方便调试。 */
					512,		  /* 堆栈深度 - 大多数小型微控制器将使用比这少得多的堆栈。 */
					(void*)pcTextForTask1,	/* 将要打印的文本作为任务参数传递。 */
					1,			            /* 此任务将以优先级 1 运行。 */
					NULL );		            /* 在此没有使用任务句柄。 */

	/* 以完全相同的方式创建另一个任务。 请注意，这次创建的是相同的任务，
    但传入了不同的参数。 正在创建单个任务实现的两个实例。 */
	xTaskCreate( vTaskFunction, "Task 2", 512, (void*)pcTextForTask2, 1, NULL );

	/* 启动调度程序以开始执行任务。 */
	vTaskStartScheduler();	

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

例程入口函数功能也很简单，就是创建了两个任务后，启动调度器。



**知识点1**：xTaskCreate()创建多个任务时，不同的任务实例，可以使用同一个任务函数，即第一个参数可以相同。

**知识点2**：xTaskCreate()创建任务时，可以传递参数给要创建的任务，使用第四个参数。



The End.

2023-04-05.