## FreeRTOS学习笔记[5]Example003-优先级实验(Lu)

本篇主要是学习使用xTaskCreate()创建任务时指定任务优先级。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example003

把其中的main.c文件改为Example003.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example003分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example003/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example003

主要修改：

1.Example003.c中的main()函数名称改为 Example003()，然后在工程main.c文件中声明，并在初始化完成后调用 Example003()；

2.任务栈大小从1000改为512，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example003.c添加到工程，然后编译。

下载运行，大概每2秒输出一个字符串。

![1680704905993](image/1680704905993.png)

#### Example003代码分析

```
void vTaskFunction( void *pvParameters )
{
char *pcTaskName;
volatile uint32_t ul;

	/* 要打印的字符串是通过参数传入的。 将此转换为字符指针。 */
	pcTaskName = ( char * ) pvParameters;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 打印出此任务的名称。 */
		vPrintString( pcTaskName );

		/* 延迟一段时间。 */
		for( ul = 0; ul < mainDELAY_LOOP_COUNT; ul++ )
		{
			/* 这个循环只是一个非常粗糙的延迟实现。 这里没有做任何事情。 
			后面的练习将用适当的延迟/睡眠功能替换这个粗略的循环。 */
		}
	}
}
```

两个任务使用相同的函数，都是无限循环中打印输出一个字符，延时使用for()循环实现。

通过pvParameters获取创建时传入的参数。因为传入的是字符串，所以需要强制转换成字符串再使用。



```
/* 定义将作为任务参数传入的字符串。 这些被定义为 const 并在堆栈外，以确保它们在任务执行时保持有效。 */
const char *pcTextForTask1 = "Task 1 is running\r\n";
const char *pcTextForTask2 = "Task 2 is running\r\n";

/*-----------------------------------------------------------*/

int Example003( void )
{
	/* 创建优先级为 1 的第一个任务... */
	xTaskCreate( vTaskFunction, "Task 1", 512, (void*)pcTextForTask1, 1, NULL );

	/* ...以及优先级为 2 的第二个任务。优先级是倒数第二个参数。 */
	xTaskCreate( vTaskFunction, "Task 2", 512, (void*)pcTextForTask2, 2, NULL );

	/* 启动调度程序以开始执行任务。 */
	vTaskStartScheduler();	

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

例程入口函数功能也很简单，就是创建了两个任务后，启动调度器。



可以发现，只有Task2运行，Task1没有得到运行。

因为在任务函数中，并没有主动让出CPU的API。导致优先级高的Task2一直独占CPU，Task1没有机会运行。



**知识点1**：xTaskCreate()创建任务时，可以指定任务的优先级，使用倒数第二个参数，数值越大优	先级越高，0是最低优先级，一般保留给空闲任务。

**知识点2**：高优先级的任务不进入阻塞/挂起状态，低优先级的任务将没有机会运行。



The End.

2023-04-05.