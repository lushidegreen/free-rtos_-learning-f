## FreeRTOS学习笔记[6]Example004-使用“阻塞”状态产生延时(Lu)

本篇主要是学习使用vTaskDelay()实现延时。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example004

把其中的main.c文件改为Example004.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example004分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example004/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example004

主要修改：

1.Example004.c中的main()函数名称改为 Example004()，然后在工程main.c文件中声明，并在初始化完成后调用 Example004()；

2.任务栈大小从1000改为512，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example004.c添加到工程，然后编译。

下载运行，每250ms输出两个字符串。

![1680789556427](image/1680789556427.png)

#### Example004代码分析

```
void vTaskFunction( void *pvParameters )
{
char *pcTaskName;
const TickType_t xDelay250ms = pdMS_TO_TICKS( 250UL );

	/* 要打印的字符串是通过参数传入的。 将此转换为字符指针。 */
	pcTaskName = ( char * ) pvParameters;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 打印出此任务的名称。 */
		vPrintString( pcTaskName );

		/* 延迟一段时间。  这次调用 vTaskDelay() 将任务置于阻塞状态，直到延迟期到期。 
        该参数需要在 'ticks' 中指定的时间，并且使用 pdMS_TO_TICKS() 宏
        （其中声明了 xDelay250ms 常量）将 250 毫秒转换为等效的滴答时间。 */
		vTaskDelay( xDelay250ms );
	}
}
```

两个任务使用相同的函数，都是无限循环中打印输出一个字符，调用vTaskDelay()进行延时。

通过pvParameters获取创建时传入的参数。因为传入的是字符串，所以需要强制转换成字符串再使用。



```
/* 定义将作为任务参数传入的字符串。 这些被定义为 const 并在堆栈外，以确保它们在任务执行时保持有效。 */
const char *pcTextForTask1 = "Task 1 is running\r\n";
const char *pcTextForTask2 = "Task 2 is running\r\n";

/*-----------------------------------------------------------*/

int Example004( void )
{
	/* 创建优先级为 1 的第一个任务... */
	xTaskCreate( vTaskFunction, "Task 1", 512, (void*)pcTextForTask1, 1, NULL );

	/* ...以及优先级为 2 的第二个任务。优先级是倒数第二个参数。 */
	xTaskCreate( vTaskFunction, "Task 2", 512, (void*)pcTextForTask2, 2, NULL );

	/* 启动调度程序以开始执行任务。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

例程入口函数功能也很简单，就是创建了两个任务后，启动调度器。



可知Task 2和Task 1任务都得到了运行。

虽然Task 2比Task 1优先级高，但是当Task 2调用vTaskDelay()时，就进入了阻塞状态，让出了CPU，Task 1就有机会运行了。



**知识点**：任务的4种状态：运行、阻塞、挂起、就绪。

它们之间的转换关系如下：

![1680790075834](image\1680790075834.png)

The End.

2023-04-06.