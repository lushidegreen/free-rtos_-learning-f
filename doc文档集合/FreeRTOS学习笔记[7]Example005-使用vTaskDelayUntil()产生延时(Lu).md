## FreeRTOS学习笔记[7]Example005-使用vTaskDelayUntil()产生延时(Lu)

本篇主要是学习使用vTaskDelay()实现延时。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example005

把其中的main.c文件改为Example005.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example005分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example005/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example005

主要修改：

1.Example005.c中的main()函数名称改为 Example005()，然后在工程main.c文件中声明，并在初始化完成后调用 Example005()；

2.任务栈大小从1000改为512，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example005.c添加到工程，然后编译。

下载运行，每250ms输出两个字符串。

![1680790928483](image\1680790928483.png)

#### Example005代码分析

```
void vTaskFunction( void *pvParameters )
{
char *pcTaskName;
TickType_t xLastWakeTime;
const TickType_t xDelay250ms = pdMS_TO_TICKS( 250UL );

	/* 要打印的字符串是通过参数传入的。 将此转换为字符指针。 */
	pcTaskName = ( char * ) pvParameters;

	/* xLastWakeTime 变量需要使用当前滴答计数进行初始化。 
    请注意，这是唯一一次访问此变量。 
    从此时起，xLastWakeTime 由 vTaskDelayUntil() API 函数自动管理。 */
	xLastWakeTime = xTaskGetTickCount();

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 打印出此任务的名称。 */
		vPrintString( pcTaskName );

		/* 希望此任务每 250 毫秒准确执行一次。 
        因为vTaskDelayUntil()函数，时间以滴答为单位测量，并且pdMS_TO_TICKS()宏用于将其转换为毫秒。 
        xLastWakeTime 在 vTaskDelayUntil() 中自动更新，因此不必通过此任务代码进行更新。 */
		vTaskDelayUntil( &xLastWakeTime, xDelay250ms );
	}
}
```

两个任务使用相同的函数，都是无限循环中打印输出一个字符，调用vTaskDelayUntil()进行延时。



```
/* 定义将作为任务参数传入的字符串。 这些被定义为 const 并在堆栈外，以确保它们在任务执行时保持有效。 */
const char *pcTextForTask1 = "Task 1 is running\r\n";
const char *pcTextForTask2 = "Task 2 is running\r\n";

/*-----------------------------------------------------------*/

int Example005( void )
{
	/* 创建优先级为 1 的第一个任务... */
	xTaskCreate( vTaskFunction, "Task 1", 512, (void*)pcTextForTask1, 1, NULL );

	/* ...以及优先级为 2 的第二个任务。优先级是倒数第二个参数。 */
	xTaskCreate( vTaskFunction, "Task 2", 512, (void*)pcTextForTask2, 2, NULL );

	/* 启动调度程序以开始执行任务。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

例程入口函数功能也很简单，就是创建了两个任务后，启动调度器。



可知Task 2和Task 1任务都得到了运行。

**知识点**：vTaskDelayUntil()和vTaskDelay()不同的计时方式。

举例：假设有在两个任务函数代码如下

![1680791614885](image\1680791614885.png)

可以很容易知道，这个任务是每300ms输出一次“Task 1...”

![1680791656159](image\1680791656159.png)

而这个任务，则是每200ms输出一次“Task 2...”。

因为vTaskDelayUntil()是以xLastWakeTime变量的值为计时起始时刻计算延时的；并且返回之后会把当前的Tick计数值保存到xLastWakeTime变量中，以作为下次的计算延时起始时刻。



串口输出如下：

![1680792046897](image\1680792046897.png)

可以看到，Task 2任务准确地每间隔200ms输出一个字符串；

Task 1每300ms输出一个字符串，并且不那么准确，因为以下两行代码并不包含在延时周期之内：

TickCount = xTaskGetTickCount();

 vPrintString( "Task 1...%d\r\n", TickCount);



The End.

2023-04-06.