## FreeRTOS学习笔记[8]Example006-阻塞任务和非阻塞任务的组合(Lu)

本篇主要是学习阻塞任务和非阻塞任务的行为差异。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example006

把其中的main.c文件改为Example006.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example006分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example006/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example006

主要修改：

1.Example006.c中的main()函数名称改为 Example006()，然后在工程main.c文件中声明，并在初始化完成后调用 Example006()；

2.任务栈大小从1000改为256，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

#### Step3.编译下载运行。

把Example006.c添加到工程，然后编译。

下载运行，会持续输出字符串。

![1680872760066](image\1680872760066.png)

#### Example006代码分析

```
void vContinuousProcessingTask( void *pvParameters )
{
char *pcTaskName;

	/* 要打印的字符串是通过参数传入的。 将此转换为字符指针。 */
	pcTaskName = ( char * ) pvParameters;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 打印出此任务的名称。此任务只是重复执行此操作，而不会阻塞或延迟。 */
		vPrintString( pcTaskName );
	}
}
```

这个持续任务函数，只是循环地打印输出字符串。

在循环中，没有任何主动放弃CPU控制权的函数，这是非阻塞任务的特点。



```
void vPeriodicTask( void *pvParameters )
{
TickType_t xLastWakeTime;
const TickType_t xDelay5ms = pdMS_TO_TICKS( 3UL );

	/* xLastWakeTime 变量需要使用当前滴答计数进行初始化。 
    请注意，这是唯一一次访问此变量。 
    从此时起，xLastWakeTime 由 vTaskDelayUntil() API 函数自动管理。 */
	xLastWakeTime = xTaskGetTickCount();

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 打印出此任务的名称。 */
		vPrintString( "Periodic task is running\r\n" );

		/* 希望此任务每 10 毫秒准确执行一次。 */
		vTaskDelayUntil( &xLastWakeTime, xDelay5ms );
	}
}
```

周期任务函数，每10ms输出一个字符串，然后调用vTaskDelayUntil()延时。

和上面的非阻塞函数不同，这个任务函数，打印输出字符串后，调用了一个阻塞延时函数，也就是主动放弃CPU控制权。



```
/* 定义将作为任务参数传入的字符串。 这些被定义为 const 并在堆栈外，以确保它们在任务执行时保持有效。 */
const char *pcTextForTask1 = "Continuous task 1 running\r\n";
const char *pcTextForTask2 = "Continuous task 2 running\r\n";
const char *pcTextForPeriodicTask = "Periodic task is running\r\n";

/*-----------------------------------------------------------*/

int Example006( void )
{
	/* 创建连续处理任务的两个实例，均优先级为 1。 */
	xTaskCreate( vContinuousProcessingTask, "Task 1", 256, (void*)pcTextForTask1, 1, NULL );
	xTaskCreate( vContinuousProcessingTask, "Task 2", 256, (void*)pcTextForTask2, 1, NULL );

	/* 创建优先级为 2 的周期性任务的一个实例。 */
	xTaskCreate( vPeriodicTask, "Task 3", 256, (void*)pcTextForPeriodicTask, 2, NULL );

	/* 启动调度程序以开始执行任务。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}int Example006( void )
{
	/* 创建优先级为 1 的第一个任务... */
	xTaskCreate( vTaskFunction, "Task 1", 512, (void*)pcTextForTask1, 1, NULL );

	/* ...以及优先级为 2 的第二个任务。优先级是倒数第二个参数。 */
	xTaskCreate( vTaskFunction, "Task 2", 512, (void*)pcTextForTask2, 2, NULL );

	/* 启动调度程序以开始执行任务。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

例程入口函数功能也很简单，就是创建了三个任务后，启动调度器。

两个持续打印的函数，使用相同的优先级，且优先级较低。另一个周期任务则优先级较高。



**知识点**：configUSE_TIME_SLICING配置为1，使能时间片调度。则优先级相同的任务有同等的运行机会。默认configUSE_TIME_SLICING配置为1。

本例程中，Task 1和Task 2优先级相同，且都是非阻塞函数，所以它们轮流运行。



The End.

2023-04-07.