## FreeRTOS学习笔记[9]Example007-定义空闲任务钩子函数(Lu)

本篇主要是学习使用空闲任务钩子函数。

#### Step1.从官方例程中，复制源文件到工程

路径.\source-code-for-book-examples\Win32-simulator-MSVC\Examples\Example007

把其中的main.c文件改为Example007.c并放到工程目录\FreeRTOS_proj\Core\Src

#### Step2.修改源码。

修改后的代码，见Example007分支。

查看地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/tree/Example007/

压缩包下载地址：https://gitee.com/lushidegreen/free-rtos_-learning-f/repository/archive/Example007

主要修改：

1.Example007.c中的main()函数名称改为 Example007()，然后在工程main.c文件中声明，并在初始化完成后调用 Example007()；

2.任务栈大小从1000改为512，因为当前分配给FreeRTOS的堆只有5kB。如果任务栈太大，会导致创建任务失败。

3.在FreeRTOSConfig.h中配置configUSE_IDLE_HOOK为1，使能空闲任务钩子函数。

#### Step3.编译下载运行。

把Example007.c添加到工程，然后编译。

下载运行，每250ms输出2个字符串，以及一个变量计数值。

![1680875512409](image\1680875512409.png)

#### Example007代码分析

```
void vTaskFunction( void *pvParameters )
{
char *pcTaskName;
const TickType_t xDelay250ms = pdMS_TO_TICKS( 250UL );

	/* 要打印的字符串是通过参数传入的。 将此转换为字符指针。 */
	pcTaskName = ( char * ) pvParameters;

	/* 和大多数任务一样，此任务是在无限循环中实现的。 */
	for( ;; )
	{
		/* 打印出此任务的名称和 ulIdleCycleCount 增加的次数。 */
		vPrintStringAndNumber( pcTaskName, ulIdleCycleCount );

		/* 延迟一段时间。这次调用 vTaskDelay() 将任务置于阻塞状态，直到延迟期到期。 
        延迟时间在'ticks'中指定。 */
		vTaskDelay( xDelay250ms );
	}
}
```

这个任务函数，只是简单的打印输出字符串和一个变量的值，然后调用了一个阻塞延时函数。



```
/* 由空闲任务挂钩函数递增的变量。 */
static uint32_t ulIdleCycleCount = 0UL;

/* 定义将作为任务参数传入的字符串。 这些被定义为 const 并在堆栈外，以确保它们在任务执行时保持有效。 */
const char *pcTextForTask1 = "Task 1 is running\r\n";
const char *pcTextForTask2 = "Task 2 is running\r\n";

/*-----------------------------------------------------------*/

int Example007( void )
{
	/* 创建优先级为 1 的第一个任务... */
	xTaskCreate( vTaskFunction, "Task 1", 512, (void*)pcTextForTask1, 1, NULL );

	/* ...以及优先级为 2 的第二个任务。优先级是倒数第二个参数。 */
	xTaskCreate( vTaskFunction, "Task 2", 512, (void*)pcTextForTask2, 2, NULL );

	/* 启动调度程序以开始执行任务。 */
	vTaskStartScheduler();

	/* 永远不应到达以下行，因为 vTaskStartScheduler() 
	只有在没有足够的 FreeRTOS 堆内存可用于创建空闲和（如果已配置）定时器任务时才会返回。 
	本书正文中描述了堆管理和捕获堆耗尽的技术。 */
	for( ;; );
	return 0;
}
```

例程入口函数功能也很简单，就是创建了2个任务后，启动调度器。

两个优先级较低的任务，使用相同的任务函数，不同的优先级



空闲任务钩子函数：

```
/* 空闲钩子函数必须调用 vApplicationIdleHook()，不带参数，并返回 void。 */
void vApplicationIdleHook( void )
{
	/* 这个钩子函数除了增加一个计数器之外什么都不做。 */
	ulIdleCycleCount++;
}
```



**知识点**：

1.空闲任务只有就绪态和运行态，它的优先级最低，只有其他任务都处于非运行态时，才会运行。

2.FreeRTOS中，空闲任务钩子函数使用固定名称void vApplicationIdleHook( void )。

3.当空闲任务在运行时，就会循环执行空闲任务钩子函数。

4.空闲任务钩子函数中，不能有任何阻塞函数调用，如vTaskDelay()等。

5.通过配置configUSE_IDLE_HOOK为1，使能空闲任务钩子函数的调用。



The End.

2023-04-07.